import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class EnterLlarg {
	
	private List<Integer> bigint = new ArrayList<Integer>();
	
	public EnterLlarg (String numero){
		int buffer;
		char[] chararray = numero.toCharArray();
		for (int i=0; i<chararray.length; i++){
			buffer = Character.getNumericValue(chararray[i]);
			bigint.add(buffer);
		}
			
	}
	
	public EnterLlarg (long numero){
		this(String.valueOf(numero));
	}
	
	public List<Integer> getBigint() {
		return bigint;
	}

	public void setBigint(List<Integer> bigint) {
		this.bigint = bigint;
	}

	public EnterLlarg (byte[] numero){
		this(new String(numero));
	}
	
	@Override
	public String toString(){
		String output = "Numero: ";
		ListIterator<Integer> itr = bigint.listIterator();
		while (itr.hasNext())
			output += itr.next();
		
		return output;
	}
	
	public static List<Integer> suma (EnterLlarg big1, EnterLlarg big2){
		
		List<Integer> primer = big1.getBigint();
		List<Integer> segon = big1.getBigint();
		List<Integer> output = new ArrayList<Integer>();
		List<Integer> llarg;
		List<Integer> curt;
		int numero1;
		int numero2;
		int suma=0;
		
		if (primer.size()>segon.size()){
			llarg = primer;
			curt = segon;
		}else{
			llarg = segon;
			curt = primer;
		}
		
		ListIterator<Integer> largo = llarg.listIterator(llarg.size());
		ListIterator<Integer> corto = curt.listIterator(curt.size());
		
		
		while(largo.hasPrevious()){
			if (corto.hasPrevious()){
				numero1 = largo.previous();
				numero2 = corto.previous();
				
				suma += numero1+numero2;
				
				if(suma >=10){
					output.add(0, suma-10);
					suma = 1;
				}else{
					output.add(0, suma);
					suma = 0;
				}
				
			}else{
				numero1 = largo.previous();
				
				suma += numero1;
				
				if(suma >=10){
					output.add(0, suma-10);
					suma = 1;
				}else{
					output.add(0, suma);
					suma = 0;
				}
				
			}
		}
		if(suma == 1)
			output.add(0, suma);
		
		return output;
	}
}
