import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		String numero1;
		String buffer;
		byte[] numero2;
		Scanner in = new Scanner(System.in);
		
		//Demanem els dos bigints...
		System.out.println("Entra el primer n�mero: ");
		numero1 = in.nextLine();
		
		System.out.println("Entra el segon n�mero: ");
		buffer = in.nextLine();
		
		//Comprovem que funcionen b� tots el constructors convertint el string primer a un array de bytes per cridar tots els constructors:
		
		numero2 = buffer.getBytes();
		
		//Cridem els constructors...
		EnterLlarg num1 = new EnterLlarg(numero1);
		EnterLlarg num2 = new EnterLlarg(numero2);
		
		//Mostrem els dos BigInts...
		System.out.println(num1.toString());
		System.out.println(num2.toString());
		
		List<Integer> bigint = new ArrayList<Integer>();
		
		//Sumem els dos bigintegers...
		bigint = EnterLlarg.suma(num1,num2);
		
		
		//Els mostrem
		System.out.println(bigint.toString());
		in.close();
	}

}
