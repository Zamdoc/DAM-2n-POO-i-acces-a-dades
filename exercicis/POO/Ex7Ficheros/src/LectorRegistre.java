import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class LectorRegistre {
	private static final Integer MIDA_PAIS = 174;
	private static final Integer MIDA_ID = 4;
	private static final Integer MIDA_NOM = 80;
	private static final Integer MIDA_CODIISO = 6;
	private static final Integer MIDA_CAPITAL = 80;
	private static final Integer MIDA_POBLACIO = 4;

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i = 0; i < nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static void main(String[] args) {
		String canvi, nom, capital, codiISO;
		StringBuilder nom2 = new StringBuilder();
		StringBuilder capital2 = new StringBuilder();
		StringBuilder codiISO2 = new StringBuilder();
		
		capital2.setLength(40);
		codiISO2.setLength(3);
		int id, poblacio;
		long pos = 0;
		Scanner scanner = new Scanner(System.in);

		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
			LectorRandomAccessFile.llegirFitxer(fitxer);
			System.out.print("Introdueix n�mero de registre: ");
			id = scanner.nextInt();
			scanner.nextLine();
			// per accedir a un registre multipliquem per la mida de
			// cada registre.
			pos = (id - 1) * 174;

			if (pos < 0 || pos >= fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");
			
			fitxer.seek(pos);

			fitxer.readInt(); // Saltem l'id
			nom = readChars(fitxer, 40);
			codiISO = readChars(fitxer, 3);
			capital = readChars(fitxer, 40);
			poblacio = fitxer.readInt();
			System.out.println("Pa�s: " + nom + " codiISO: "+codiISO+" Capital: "+capital+", poblaci� actual: " + poblacio);
			/* Per a l'exercici 2, s'hauria d'eliminar aquesta part de codi*/
			System.out.println("Introdueix el camp a canviar: ");
			canvi = scanner.nextLine();
			/*Fins aqui*/
			/*Eliminar tots els if, ja que volem canviar tots els camps*/
			if (canvi.equals("Pais")){
				System.out.print("Escriu el nou nom: ");
				nom = scanner.nextLine();
				pos = fitxer.getFilePointer() - MIDA_POBLACIO - MIDA_CAPITAL - MIDA_CODIISO - MIDA_NOM;
				nom2.append(nom);
				nom2.setLength(40);
				fitxer.seek(pos);
				fitxer.writeChars(nom2.toString());
			}
			
			if(canvi.equals("codiISO")){
				System.out.print("Escriu el nou codiISO: ");
				codiISO = scanner.nextLine();
				codiISO2.append(codiISO);
				codiISO2.setLength(40);
				pos = fitxer.getFilePointer() - MIDA_POBLACIO - MIDA_CAPITAL - MIDA_CODIISO;
				fitxer.seek(pos);
				fitxer.writeChars(codiISO2.toString());
			}

			if (canvi.equals("Capital")){
				System.out.print("Escriu la nova capital: ");
				capital = scanner.nextLine();
				capital2.append(capital);
				capital2.setLength(40);
				pos = fitxer.getFilePointer() - MIDA_POBLACIO - MIDA_CAPITAL;
				fitxer.seek(pos);
				fitxer.writeChars(capital2.toString());
			}
			
			if (canvi.equals("poblacio")) {
				System.out.print("Escriu la nova poblacio: ");
				poblacio = scanner.nextInt();
				pos = fitxer.getFilePointer() - MIDA_POBLACIO; // tornem enrere per sobreescriure la poblaci�
				fitxer.seek(pos);
				fitxer.writeInt(poblacio);
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();
	}
}