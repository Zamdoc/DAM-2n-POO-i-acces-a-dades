import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ex2Con {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("EEUU");
		MongoCollection<Document> collection = database.getCollection("zips");
		
		Bson ordenar = orderBy(descending("pop"));
		
		collection.find().sort(ordenar).limit(10).forEach((Document doc) -> System.out.println(doc.toJson()));
		
		client.close();
	}

}
