import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class Ex1 {

	public static void main(String[] args) {
		List<Actor> actors = new ArrayList<Actor>();
		
		actors.add(new Actor(5, "LOLLOBRIGIDA", "JOHNNY"));
		actors.add(new Actor(27, "MCQUEEN", "JULIA"));
		actors.add(new Actor(37, "BOLGER", "VAL"));
		actors.add(new Actor(43, "JOVOVICH", "KIRK"));
		actors.add(new Actor(84, "PITT", "JAMES"));
		actors.add(new Actor(104, "CRONYN", "PENELOPE"));
		
		List<Document> documentList = new ArrayList<Document>();
		
		for (Actor actor : actors){
			Document buffer = new Document()
					.append("actorId", actor.getId())
					.append("Last name", actor.getLastname())
					.append("First name", actor.getFirstname());
			documentList.add(buffer);
		}
		
		Document document = new Document()
			.append("_id", 19)
			.append("Title", "AMADEUS HOLY")
			.append("Description", "A Emotional Display of a Pioneer And a Technical Writer who must Battle a Man in A Baloon")
			.append("Length", 113)
			.append("Rating", "PG")
			.append("Special Features", "Commentaries,Deleted Scenes,Behind the Scenes")
			.append("Rental Duration", 6)
			.append("Replacement Cost", 20.99)
			.append("Category", "Action")
			.append("Actors", documentList);
		
		JsonWriterSettings settings = new JsonWriterSettings(true);
		System.out.println(document.toJson(settings));
	}

}
