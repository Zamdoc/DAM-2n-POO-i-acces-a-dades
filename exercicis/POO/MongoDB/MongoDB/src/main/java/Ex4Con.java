import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ex4Con {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("EEUU");
		MongoCollection<Document> collection = database.getCollection("zips");
		
		Bson filter = lt("pop",50);
		
		Bson order = orderBy(descending("pop"));
		
		collection.find(filter).sort(order).forEach((Document doc) -> System.out.println(doc.getString("city")+" "+doc.getInteger("pop")));
		
		client.close();
	}

}
