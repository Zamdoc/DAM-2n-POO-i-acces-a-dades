import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Examen2 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("restaurants");
		MongoCollection<Document> collection = database.getCollection("restaurants");
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introdueix un codi postal: ");
		String codipostal = in.nextLine();
		Bson projection = fields(include("name","address.street","cuisine","grades.score"), excludeId());
		
		List<Document> results = collection.aggregate(asList(
				match(eq("address.zipcode", codipostal)),
				unwind("$grades"),
				project(projection),
				group(new Document("_id","$_id").append("name", "$name").append("cuisine","$cuisine").append("grades.score","$grades.score"))
				)).into(new ArrayList<>());
		
		for (Document doc : results) {
		    System.out.println(doc.toJson());
		}
		
		in.close();
		client.close();
	}

}
