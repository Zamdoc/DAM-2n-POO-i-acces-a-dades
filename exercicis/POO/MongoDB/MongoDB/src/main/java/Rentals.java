import java.util.List;

import org.bson.Document;

public class Rentals {
	private Integer rentalId;
	private String rentalDate;
	private String returnDate;
	private Integer staffId;
	private Integer filmId;
	private String filmTitle;
	private List<Document> payments;
	
	public Rentals(Integer rentalId, String rentalDate, String returnDate, Integer staffId, Integer filmId,
			String filmTitle, List<Document> payments) {
		super();
		this.rentalId = rentalId;
		this.rentalDate = rentalDate;
		this.returnDate = returnDate;
		this.staffId = staffId;
		this.filmId = filmId;
		this.filmTitle = filmTitle;
		this.payments = payments;
	}
	public Integer getRentalId() {
		return rentalId;
	}
	public void setRentalId(Integer rentalId) {
		this.rentalId = rentalId;
	}
	public String getRentalDate() {
		return rentalDate;
	}
	public void setRentalDate(String rentalDate) {
		this.rentalDate = rentalDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public Integer getFilmId() {
		return filmId;
	}
	public void setFilmId(Integer filmId) {
		this.filmId = filmId;
	}
	public String getFilmTitle() {
		return filmTitle;
	}
	public void setFilmTitle(String filmTitle) {
		this.filmTitle = filmTitle;
	}
	public List<Document> getPayments() {
		return payments;
	}
	public void setPayments(List<Document> payments) {
		this.payments = payments;
	}
}
