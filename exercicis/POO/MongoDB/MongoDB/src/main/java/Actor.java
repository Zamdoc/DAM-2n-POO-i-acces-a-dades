
public class Actor {
	
	private int id;
	private String lastname;
	
	public Actor(int id, String cog, String nom){
		this.id = id;
		this.lastname = cog;
		this.firstname = nom;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	private String firstname;
	
}
