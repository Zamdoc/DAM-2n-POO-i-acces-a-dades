import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ex5Con {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("EEUU");
		MongoCollection<Document> collection = database.getCollection("zips");
		
		Bson sortDes = orderBy(descending("_id"));
		Bson sortAsc = orderBy(ascending("_id"));
		
		collection.distinct("state", String.class).forEach((String state) -> {
			Bson filter = eq("state", state);
			System.out.println(state);
			collection.find(filter).sort(sortAsc).limit(1).forEach((Document doc) -> 
				System.out.println(doc.getInteger("_id")+" "+doc.getString("city")));
			collection.find(filter).sort(sortDes).limit(1).forEach((Document doc) -> 
				System.out.println(doc.getInteger("_id")+" "+doc.getString("city")));
		});
		
		client.close();
	}
}
