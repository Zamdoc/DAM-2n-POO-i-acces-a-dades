
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ex1Ins {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("EEUU");
		MongoCollection<Document> coll = database.getCollection("zips");
		coll.drop();
		Document doc = null;
		
		String frase;

		long start = 0;
		long end = 0;
		
		File file = new File("D:\\Kevin\\DAM-2n-POO-i-acces-a-dades\\exercicis\\POO\\MongoDB\\MongoDB\\json.json");
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			start = System.currentTimeMillis();
			while ((frase = reader.readLine()) != null) {
				
				frase = frase.replaceAll(" : ", " ");
				frase = frase.replaceAll(", ", " ");
				frase = frase.replaceAll("\"", " ");
				frase = frase.replace(" [ ", " ");
				frase = frase.replace(" ] ", " ");
				frase = frase.replace("{  ", "");
				frase = frase.replace("  }", "");
				String[] s = frase.split("  ");
				
				doc = new Document().append("_id", Integer.parseInt(s[1].trim())).append("city", s[3].trim())
						.append("loc", Arrays.asList(s[5].split(" "))).append("pop", Integer.parseInt(s[7].trim()))
						.append("state", s[9].trim());
				coll.insertOne(doc);
				System.out.println(doc.toJson());
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		end = System.currentTimeMillis();

		System.out.println("Tiempo total: " + Long.valueOf(end - start) + "milisegundos");
		try {
			
			Thread.sleep(6000);
			for (Document x : coll.find()) 
				System.out.println(x.toJson());
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		client.close();
	}
}
