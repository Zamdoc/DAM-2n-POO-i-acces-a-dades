import static com.mongodb.client.model.Filters.eq;

import java.util.concurrent.ThreadLocalRandom;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

public class Examen3 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("imatges");
		MongoCollection<Document> collection = database.getCollection("imatges");
		
		MongoCursor<Document> cursor = collection.find().iterator();
		
		while (cursor.hasNext()){
			Document doc = cursor.next();
			int numero = ThreadLocalRandom.current().nextInt(0, 100);
			int id = Integer.parseInt(doc.get("_id").toString());
			collection.updateOne(eq("_id", id), new Document("$set", new Document("likes", numero)), new UpdateOptions().upsert(true));
		}
		
		client.close();
	}

}
