
import java.util.Scanner;
import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class Ex1Con {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("EEUU");
		MongoCollection<Document> collection = db.getCollection("zips");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Ciudad a buscar: ");
		
		String ciudad = in.nextLine();

		Bson f = Filters.eq("city", ciudad);

		collection.find(f).forEach((Document doc) -> System.out.println(doc.toJson()));
		
		client.close();
		in.close();
	}

}