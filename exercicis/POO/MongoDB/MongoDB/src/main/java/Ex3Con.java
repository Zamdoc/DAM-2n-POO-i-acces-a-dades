import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ex3Con {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("EEUU");
		MongoCollection<Document> collection = database.getCollection("zips");
		
		Bson order = orderBy(descending("loc.1"));
		
		long i = collection.count();
		
		collection.find().sort(order).skip((int)i/2).limit(1).forEach((Document doc) -> System.out.println(doc.toJson()));
		
		client.close();
	}

}
