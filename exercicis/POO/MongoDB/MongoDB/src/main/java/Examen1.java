import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Examen1 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase database = client.getDatabase("restaurants");
		MongoCollection<Document> collection = database.getCollection("restaurants");
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introdueix un codi postal: ");
		String codipostal = in.nextLine();
		
		Bson filter = and(eq("address.zipcode", codipostal), eq("cuisine", "Vegetarian"));
		Bson projection = fields(include("name","address.street"), excludeId());
		
		System.out.println("Restaurants vegetariants en aquest codi postal: ");
		
		try (MongoCursor<Document> cursor = collection.find(filter).projection(projection).iterator()) {
            while (cursor.hasNext()) {
                Document doc = cursor.next();
                Document address = (Document) doc.get("address");
                System.out.println("Nom: " + doc.getString("name") + " Adre�a: " + address.get("street"));
            }
        }

		client.close();
		in.close();
	}

}
