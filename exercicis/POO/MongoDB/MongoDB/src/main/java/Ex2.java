import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class Ex2 {

	public static void main(String[] args) {
		
		List<Payments> payments = new ArrayList<Payments>();

		payments.add(new Payments(3, 6.00, "2005-06-15 00:54:12.0"));
		payments.add(new Payments(5, 10.00, "2005-06-15 21:08:46.0"));
		payments.add(new Payments(6, 5.00, "2005-06-16 15:18:57.0"));
		

		List<Document> paymentList = new ArrayList<Document>();
		List<Document> paymentList2 = new ArrayList<Document>();
		List<Document> paymentList3 = new ArrayList<Document>();
		
		
		Document buffer = new Document()
				.append("Payment Id", payments.get(0).getPaymentId())
				.append("Amount", payments.get(0).getAmount())
				.append("Payment Date", payments.get(0).getPaymentDate());
		paymentList.add(buffer);
		buffer = new Document()
				.append("Payment Id", payments.get(1).getPaymentId())
				.append("Amount", payments.get(1).getAmount())
				.append("Payment Date", payments.get(1).getPaymentDate());
		paymentList2.add(buffer);
		buffer = new Document()
				.append("Payment Id", payments.get(2).getPaymentId())
				.append("Amount", payments.get(2).getAmount())
				.append("Payment Date", payments.get(2).getPaymentDate());
		paymentList3.add(buffer);
		
		
		List<Rentals> rentals = new ArrayList<>();
		
		rentals.add(new Rentals( 1185, "2005-06-15 00:54:12.0", 
				"2005-06-23 02:42:12.0", 2, 611, "MUSKETEERS WAIT", 
				paymentList));
		
		rentals.add(new Rentals(1476, "2005-06-15 21:08:46.0",
				"2005-06-25 02:26:46.0", 1, 308, "FERRIS MOTHER", 
				paymentList2));
		
		rentals.add(new Rentals(1725, "2005-06-16 15:18:57.0",
				"2005-06-17 21:05:57.0", 1, 159, "CLOSER BANG", 
				paymentList3));
		
		List<Document> documentList = new ArrayList<Document>();
		
		for (Rentals rental : rentals){
			Document buffer2 = new Document()
					.append("rentalId", rental.getRentalId())
					.append("Rental Date", rental.getRentalDate())
					.append("Return Date", rental.getReturnDate())
					.append("staffId", rental.getStaffId())
					.append("filmId", rental.getFilmId())
					.append("Film Title", rental.getFilmTitle())
					.append("Payments", rental.getPayments());
			
			documentList.add(buffer2);
		}
		
		Document doc = new Document()
				.append("_id", 1)
				.append("First Name", "MARY")
				.append("Last Name", "SMITH")
				.append("Address", "1913 Hanoi Way")
				.append("District", "Nagasaki")
				.append("City", "Sasebo")
				.append("Country", "Japan")
				.append("Phone", "28303384290")
				.append("Rentals", documentList);
		
		JsonWriterSettings settings = new JsonWriterSettings(true);
		System.out.println(doc.toJson(settings));
	}
}
