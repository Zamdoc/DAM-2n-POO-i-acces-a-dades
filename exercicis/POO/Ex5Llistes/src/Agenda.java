import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Agenda {
	private List<Contacte> llista = new ArrayList<Contacte>();
	private static Scanner in = new Scanner(System.in);
	private ListIterator<Contacte> itr;
	private Contacte mostrar;
	private Contacte buffer;
	
	//Demana les dades d'un contacte per pantalla i les retorna.
	public Contacte guardarDades(Contacte contacte){
		String dades;
		contacte = new Contacte();
		System.out.println("Opci� 1: Afegir element.");
		System.out.print("Entra nom: ");
		dades = in.nextLine();
		contacte.setNom(dades);
		
		System.out.print("Entra cognoms: ");
		dades = in.nextLine();
		contacte.setCognoms(dades);
		
		System.out.print("Entra direccio: ");
		dades = in.nextLine();
		contacte.setDireccio(dades);
		
		System.out.print("Entra telefon: ");
		dades = in.nextLine();
		contacte.setTelefon(dades);
		
		return contacte;
	}
	//Afegeix el contacte que li pasen per par�metre a l'agenda. L'afegeix ja ordenat.
	public void afegirContacte (Contacte contacte){
		boolean alfinal = true;
		
		try{
			itr = llista.listIterator();
			while (itr.hasNext() && alfinal == true){
				//Compara per cognom.
				if (contacte.getCognoms().compareTo((buffer = itr.next()).getCognoms()) < 0){
					itr.previous();
					itr.add(contacte);
					alfinal = false;
				//Si els congnoms son iguals, entra aqui.
				}else if (contacte.getCognoms().compareTo(buffer.getCognoms()) == 0)
					//Compara per nom
					if (contacte.getNom().compareTo(buffer.getNom()) < 0){
						itr.previous();
						itr.add(contacte);
						alfinal = false;
					}
			}
			if (alfinal == true)
				itr.add(contacte);
			}
		//No ha d'arribar aqui.
		catch(Exception a){
			System.out.println("No s'ha pogut afegir");
		}
	}
	//Elimina els contactes que coincideixen amb el string que li han donat per par�metre
	public void eliminarContacte(String erase){
		boolean done = false;
		try{
			itr = llista.listIterator();
			while (itr.hasNext() && done == false){
				if (erase.equals((buffer = itr.next()).getNom())){
					itr.remove();
					done = true;
				}
			}
			System.out.println("Contacte "+erase+" eliminat.");
		}
		//No ha d'arribar aqui.
		catch(Exception a){
			System.out.println("No s'ha pogut eliminar");
		}
		
	}
	
	//Mostra els contactes de l'agenda.
	public void mostraContactes(){
		
		System.out.println("Llista de contactes: ");
		itr = llista.listIterator();
		while(itr.hasNext()){
			mostrar = itr.next();
			System.out.println("Contacte "+itr.nextIndex()+": "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
		}
	}
	//Modifica un valor del contacte. Es rebr� un index per saber on hi es el contacte a modificar.
	void modificaContacte(int index){
		index--;
		int opcio;
		String cambi;
		try{
			itr = llista.listIterator(index);
			//Menu per modificar.
			System.out.println("-----------------------------------");
			System.out.println("Opci� 1: Modificar nom.");
			System.out.println("Opci� 2: Modificar cognom.");
			System.out.println("Opci� 3: Modificar direccio.");
			System.out.println("Opci� 4: Modificar telefon.");
			System.out.println("-----------------------------------");
			System.out.print("Entra l'opci� desitjada: ");
			opcio = in.nextInt();
			in.nextLine();
			//Segons l'opci� escollida, modificarem una dada o una altra.
			switch(opcio){
				//Modifiquem el nom.
				case 1:
					
					System.out.println("Opci� 1: Modificar nom.");
					System.out.print("Nou nom: ");
					cambi = in.nextLine();
					mostrar = llista.get(index);
					mostrar.setNom(cambi);
					llista.set(index, mostrar);
					System.out.println("Contacte "+(itr.nextIndex()+1)+" modificat: "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
					break;
				//Modifiquem el cognom.
				case 2:
					System.out.println("Opci� 2: Modificar cognom.");
					System.out.print("Nou cognom: ");
					cambi = in.nextLine();
					mostrar.setCognoms(cambi);
					System.out.println("Contacte "+(itr.nextIndex()+1)+" modificat: "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
					break;
				//Modifiquem la direccio.
				case 3:
					System.out.println("Opci� 3: Modificar direccio.");
					System.out.print("Nova direccio: ");
					cambi = in.nextLine();
					mostrar.setDireccio(cambi);
					System.out.println("Contacte "+(itr.nextIndex()+1)+" modificat: "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
					break;
				//Modifiquem el tel�fon.
				case 4:
					System.out.println("Opci� 4: Modificar telefon.");
					System.out.print("Nou telefon: ");
					cambi = in.nextLine();
					mostrar.setTelefon(cambi);
					System.out.println("Contacte "+(itr.nextIndex()+1)+" modificat: "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
					break;
					
			}
		}
		//No ha d'arribar aqui.
		catch(Exception a){
			System.out.println("No s'ha pogut modificar");
		}
	}
	
	
	public void buscaPersona(String cognom){
		
		System.out.println("Llista de contactes amb el cognom"+ cognom+": ");
		itr = llista.listIterator();
		while(itr.hasNext()){
			mostrar = itr.next();
			
			if(mostrar.getCognoms().equals(cognom))
				System.out.println("Contacte "+itr.nextIndex()+": "+mostrar.getNom()+", "+mostrar.getCognoms()+", "+mostrar.getDireccio()+", "+mostrar.getTelefon());
		}
		
	}
}
