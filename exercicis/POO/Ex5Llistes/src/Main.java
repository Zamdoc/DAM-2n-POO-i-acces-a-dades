import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);
	public static int menu() {
		
		int eleccio;
		System.out.println("-----------------------------------");
		System.out.println("Opci� 1: Afegir contacte.");
		System.out.println("Opci� 2: Esborrar contacte.");
		System.out.println("Opci� 3: Modificar contacte.");
		System.out.println("Opci� 4: Cercar contacte.");
		System.out.println("Opci� 5: Mostrar contacte.");
		System.out.println("Opci� 0: Sortir.");
		System.out.println("-----------------------------------");
		System.out.print("Entra l'opci� desitjada: ");
		eleccio = in.nextInt();
		in.nextLine();
		return eleccio;
	}

	public static void main(String[] args) {
		Agenda agenda = new Agenda();
		int resposta;
		Contacte contacte = new Contacte();
		String erase;
		String search;
		int modindex;
		do{
			resposta = menu();
			switch (resposta) {
	
				case 1:
					contacte = agenda.guardarDades(contacte);
					agenda.afegirContacte(contacte);
					System.out.println("Contacte afegit amb les dades: "+contacte.getNom()+", "+contacte.getCognoms()+", "+contacte.getDireccio()+", "+contacte.getTelefon());
					break;
				case 2:
					System.out.println("Opci� 2: Esborrar contacte.");
					agenda.mostraContactes();
					System.out.print("Entra nom: ");
					erase = in.nextLine();
					agenda.eliminarContacte(erase);
					break;
				case 3:
					System.out.println("Opci� 3: Modificar contacte.");
					agenda.mostraContactes();
					System.out.println("Escull el n�mero de contacte a modificar: ");
					modindex = in.nextInt();
					in.nextLine();
					agenda.modificaContacte(modindex);
					break;
				case 4:
					System.out.println("Opci� 4: Cercar contacte.");
					System.out.println("Introdueix el cognom del contacte: ");
					search = in.nextLine();
					agenda.buscaPersona(search);
					break;
				case 5:
					System.out.println("Opci� 5: Mostrar contactes.");
					agenda.mostraContactes();
					break;
				case 0:
					
					break;
			}
		}while(resposta!=0);
	}

}
