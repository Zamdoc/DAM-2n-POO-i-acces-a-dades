import java.util.Scanner;

public class PintandoFractales {

	public static void main(String[] args) {
		
		Solution read = new Solution();
		int longitud;
		int resultado;
		boolean firsttime;
		Scanner in = new Scanner(System.in);
		while (read.leer()){
			firsttime = false;
			resultado = 0;
			longitud = in.nextInt();
			while(longitud != 1 || !firsttime){
				if(longitud == 1)
					firsttime = true;
				resultado+= longitud*4;
				longitud = longitud/2;
				if (firsttime)
					longitud = 1;
			}
			System.out.println(resultado);
			in.nextLine();
		}
		in.close();
	}
}
