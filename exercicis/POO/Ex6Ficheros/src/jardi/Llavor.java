package jardi;

/**
 * Aquesta classe modelitza la llavor de qualsevol planta.
 * 
 * Per evitar haver de crear una classe de llavor diferent per a cada tipus de planta,
 * s'ha fet que una llavor guardi una refer�ncia a un objecte Planta del tipus concret
 * a qu� pertany aquesta llavor. Aquest objecte Planta ser� la planta que germinar�
 * de la llavor quan aquesta maduri, i tamb� ens permet delegar alguna operaci�
 * que dep�n del tipus de planta. D'aquesta manera nom�s caldr� crear classes
 * per llavors espec�fiques quan tinguin un comportament diferent a la major part
 * de llavors.
 * 
 */
public class Llavor implements ElementJardi {

	private static final long serialVersionUID = 1L;
	/**
	 * Aquesta planta ser� la que germinar� d'aquesta llavor.
	 */
	private Planta planta;
	/**
	 * Temps de vida de la llavor. Quan arriba a 5, la llavor germina.
	 */
	private int temps = 0;
	/**
	 * Indica si la llavor �s viva o no. La llavor est� viva fins que germina.
	 */
	private boolean viva = true;
	/**
	 * El constructor de llavor rep una planta, que ser� la nova planta que
	 * germinar� quan la llavor maduri.
	 * 
	 * @param p  Planta que ha de sorgir d'aquesta llavor.
	 */
	public Llavor(Planta p) {
		planta = p;
	}
	/**
	 * La llavor romandr� viva durant cinc torns. Despr�s, naixer� la
	 * planta que t� a dins (la retornar�) i la llavor morir�.
	 * 
	 * @return  null mentre la llavor segueixi viva, la planta que ha
	 * de n�ixer quan la llavor mor.
	 */
	@Override
	public Planta temps() {
		Planta p = null;
		temps++;
		if (temps == 5) {
			p = planta;
			viva = false;
		}
		return p;
	}
	/**
	 * La llavor sempre t� al�ada 0. Retorna '.' si es demana l'al�ada 0
	 * i espai si es demana qualsevol altra al�ada.
	 * 
	 * @param altura  l'al�ada per la qual es vol con�ixer el car�cter que
	 * s'ha d'escriure per la llavor.
	 */
	@Override
	public char getChar(int altura) {
		char c = ' ';
		if (altura == 0)
			c = '.';
		return c;
	}

	@Override
	public boolean isViva() {
		return viva;
	}
	
	@Override
	public int escampa() {
		return 0;
	}
}