package jardi;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * A la classe Principal creem un jard�, hi posem alguna planta, i fem passar un
 * torn cada cop que l'usuari introdueix una l�nia de text.
 */
public class Principal {
	/**
	 * El jard� que es crea, de mida 40.
	 */
	private Jardi jardi = new Jardi(40);

	/**
	 * El main es limita a crear un objecte de tipus Principal. El constructor
	 * d'aquest objecte fa la resta. Aix� �s habitual, com que treballem amb un
	 * objecte, evita haver de declarar la resta de propietats i m�todes de la
	 * classe com static.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Principal();
	}

	/**
	 * S'inicialitza el jard�, i es demana entrada a l'usuari. Cada l�nia que
	 * s'introdueix avan�a un torn el jard�. Si l'usuari introdueix la cadena
	 * "surt" s'acaba el programa.
	 */
	public Principal() {
		Scanner sc = new Scanner(System.in);
		String ordre = "";
		jardi.posaElement(new Altibus(), 10);
		jardi.posaElement(new Declinus(), 30);

		if (Files.exists(Paths.get("jardi.sav"),LinkOption.NOFOLLOW_LINKS)) {
			try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.sav"));) {
				Object o = lector.readObject();
				if (o instanceof Jardi) {
					jardi = (Jardi) o;
				}
			} catch (EOFException ex) {
				System.out.println("Hem arribat a final de fitxer.");
			} catch (IOException ex) {
				System.err.println(ex);
			} catch (ClassNotFoundException ex) {
				System.err.println(ex);
			} 
		}
		while (!ordre.equals("surt")) {
			jardi.temps();
			System.out.println(jardi.toString());
			ordre = sc.nextLine();
		}

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("jardi.sav"));) {
			escriptor.writeObject(jardi);
		} catch (IOException ex) {
			System.err.println(ex);
		}
		sc.close();
	}

}