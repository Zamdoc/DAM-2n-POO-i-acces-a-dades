package view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.application.Application;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.When;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import model.Game;
import model.Game.State;

public class XinosApplication extends Application
			implements ChangeListener<Game.State>, EventHandler<ActionEvent> {
	private Button[] btnCoins = new Button[4];
	private Label lblInfo = new Label("Principi del joc");
	private Label lblInstructions = new Label("Quantes monedes amagues?");
	private TextField txtBid = new TextField();
	private Button btnBid = new Button("Aposta!");
	private Label[] lblPlayers = new Label[Game.N_PLAYERS];
	private Label[] lblHiddenCoins = new Label[Game.N_PLAYERS];
	private Label[] lblBets = new Label[Game.N_PLAYERS];
	private ImageView[] ivWins = new ImageView[Game.N_PLAYERS];

	private HBox bottom;
	private VBox left;

	private Image imgCoin = new Image("/res/coin-gold.png");
	private Image imgCoins = new Image("/res/coin-pile-gold.png");
	private Image imgTick = new Image("/res/green-tick.png");
	private Image imgCross = new Image("/res/red-cross.png");

	private Game game = new Game();

	public static void main(String[] args) {
		launch(args);
	}

	private Node createTopNode() {
		VBox top = new VBox();
		top.setPadding(new Insets(10));
		top.setSpacing(12);
		top.getChildren().add(lblInfo);
		top.getChildren().add(lblInstructions);
		return top;
	}

	private Node createBottomNode() {
		bottom = new HBox();
		bottom.setPadding(new Insets(15, 12, 15, 12));
		bottom.setSpacing(10);
		bottom.setAlignment(Pos.CENTER);
		bottom.getChildren().add(new Label("Aposta: "));
		txtBid.setPrefColumnCount(3);
		bottom.getChildren().add(txtBid);
		bottom.getChildren().add(btnBid);
		bottom.setDisable(true);
		return bottom;
	}

	private Node createLeftNode() {
		left = new VBox();
		left.setPadding(new Insets(10));
		left.setSpacing(5);
		
		for (int i = 0; i < 4; i++) {
			btnCoins[i] = new Button(Integer.toString(i));
			left.getChildren().add(btnCoins[i]);
			btnCoins[i].setPrefSize(100, 50);
		}
		
		return left;
	}
	
	private void createPlayerLabels(GridPane center) {
		List<String> playerNames = new ArrayList<>();
		playerNames.add("Tu");
		for (int i = 1; i < Game.N_PLAYERS; i++) {
			playerNames.add("Jugador " + i);
		}
		Iterator<String> it = playerNames.iterator();
		int col = 0;
		while (it.hasNext()) {
			lblPlayers[col] = new Label(it.next());
			center.add(lblPlayers[col], col, 0);
			col++;
		}
	}
	
	private void createHiddenCoinsLabels(GridPane center) {
		
		for (int col=0; col<Game.N_PLAYERS; col++) {
			
			ImageView moneda = new ImageView(imgCoin);
			center.add(moneda, col, 1);
			lblHiddenCoins[col] = new Label("?");
			
			center.add(lblHiddenCoins[col], col, 2);
		}
	}
	
	private void createBetLabels(GridPane center) {
		for (int col=0; col<Game.N_PLAYERS; col++) {
			ImageView ivCoins = new ImageView(imgCoins);
			center.add(ivCoins, col, 3);
			HBox boxBet = new HBox();
			
			lblBets[col] = new Label("?");
			
			ivWins[col] = new ImageView();
			
			boxBet.setAlignment(Pos.CENTER);
			boxBet.getChildren().addAll(lblBets[col], ivWins[col]);
			center.add(boxBet, col, 4);
		}
	}

	private Node createCenterNode() {
		GridPane center = new GridPane();
		// center.gridLinesVisibleProperty().set(true);
		for (int col = 0; col < Game.N_PLAYERS; col++) {
			ColumnConstraints columnConstraints = new ColumnConstraints();
			columnConstraints.setPercentWidth(100/Game.N_PLAYERS);
			columnConstraints.setHalignment(HPos.CENTER);
			center.getColumnConstraints().add(columnConstraints);
		}
		
		for (int row = 0; row < 5; row++){
			RowConstraints rowConstraints = new RowConstraints();
			rowConstraints.setPercentHeight(20);
			rowConstraints.setValignment(VPos.CENTER);
			center.getRowConstraints().add(rowConstraints);
		}
		
		
		createPlayerLabels(center);
		createHiddenCoinsLabels(center);
		createBetLabels(center);
		return center;
	}
	
	private void setBindings() {
		for (int col = 0; col < Game.N_PLAYERS; col++) {
			if (col > 0) {
				lblHiddenCoins[col].textProperty()
						.bind(
							new When(
								game.getStateProperty().isEqualTo(Game.State.STATE_END_GAME)
							).then(
								game.getPlayer(col).getHiddenCoinsProperty().asString()
							).otherwise(
								"?"
							)
						);
			} else {
				lblHiddenCoins[col].textProperty()
						.bind(
							new When(
								game.getStateProperty().isNotEqualTo(Game.State.STATE_HIDE_COINS)
							).then(
								game.getPlayer(col).getHiddenCoinsProperty().asString()
							).otherwise(
								"?"
							)
						);
			}
			lblBets[col].textProperty()
			.bind(new When(game.getStateProperty().isEqualTo(Game.State.STATE_SHOW_RESULT)
					.or(game.getStateProperty().isEqualTo(Game.State.STATE_END_GAME)))
					.then(game.getPlayer(col).getBetProperty().asString())
					.otherwise("?")
			);
			ivWins[col].imageProperty().bind(new TickImageBinding(col));
		}
	}
	
	private class TickImageBinding extends ObjectBinding<Image> {
		private int nPlayer;
		
		public TickImageBinding(int nPlayer) {
			super.bind(game.getStateProperty());
			this.nPlayer = nPlayer;
		}

		@Override
		protected Image computeValue() {
			Image value = null;
			if (game.getState() == Game.State.STATE_END_GAME) {
				if (game.getPlayer(nPlayer).getBet() == game.getHiddenCoinsCount()) {
					value = imgTick;
				} else {
					value = imgCross;
				}
			}
			return value;
		}
		
	}

	private void setEventHandlers() {
		
		btnBid.setOnAction(this);
		for (Button boton : btnCoins) {
			boton.setOnAction(this);
		}
	}

	private BorderPane createBorderPane() {
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(createTopNode());
		borderPane.setBottom(createBottomNode());
		borderPane.setLeft(createLeftNode());
		borderPane.setCenter(createCenterNode());
		return borderPane;
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane root = createBorderPane();

		setBindings();
		setEventHandlers();

		Scene scene = new Scene(root, 500, 500);

		primaryStage.setTitle("Els xinos");
		primaryStage.setScene(scene);
		primaryStage.show();

		game.getStateProperty().addListener(this);
	}

	private void showBids() {
		lblInfo.setText("Has apostat " + game.getPlayer(0).getBet());
		String text = "Ells aposten: ";
		for (int i = 1; i < Game.N_PLAYERS; i++) {
			text += game.getPlayer(i).getBet();
			if (i == Game.N_PLAYERS - 2)
				text += " i ";
			if (i < Game.N_PLAYERS - 2)
				text += ", ";
		}
		lblInstructions.setText(text);
	}

	private void showWinner() {
		String text;
		int guanyador = game.getGuanyador();
		if (guanyador == 0)
			text = "Has guanyat!";
		else if (guanyador > 0)
			text = "Ha guanyat el jugador " + guanyador + ".";
		else
			text = "No ha guanyat ning�.";
		lblInfo.setText(text + " En total hi havia " + game.getHiddenCoinsCount() + " monedes.");
	}

	@Override
	public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
		switch (newValue) {
		case STATE_HIDE_COINS:
			lblInstructions.setText("Quantes monedes amagues?");
			btnBid.setText("Aposta!");
			bottom.setDisable(true);
			left.setDisable(false);
			txtBid.setText("");
			break;
		case STATE_BID:
			lblInfo.setText("Has amagat " + game.getPlayer(0).getHiddenCoins() + " monedes");
			lblInstructions.setText("Quantes monedes hi ha?");
			bottom.setDisable(false);
			left.setDisable(true);
			break;
		case STATE_SHOW_RESULT:
			showBids();
			btnBid.setText("Resol!");
			break;
		case STATE_END_GAME:
			showWinner();
			btnBid.setText("Comen�a");
		}

	}

	private void handleCoinButtons(Button b) {
		if (game.getState() == Game.State.STATE_HIDE_COINS) {
			for (int i = 0; i < 4; i++) {
				if (b == btnCoins[i]) {
					game.setHiddenCoins(i);
					game.nextState();
				}
			}
		}
	}

	private void handleBidButton() {
		String strAposta = txtBid.getText();
		if (game.getState() == Game.State.STATE_BID) {
			try {
				if (strAposta.equals(""))
					lblInfo.setText("Has de posar un nombre");
				else {
					game.setBet(Integer.parseInt(strAposta));
					game.nextState();
				}
			} catch (NumberFormatException ex) {
				lblInfo.setText("Has de posar un nombre");
				txtBid.setText("");
			}
		} else if (game.getState() == Game.State.STATE_SHOW_RESULT ||
				game.getState() == Game.State.STATE_END_GAME) {
			game.nextState();
		}
	}

	@Override
	public void handle(ActionEvent event) {
		Button b = (Button) event.getSource();
		handleCoinButtons(b);
		if (b == btnBid) {
			handleBidButton();
		}
	}
}
