package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Game {
	// Constant per facilitar el canvi en el nombre de jugadors
	public static final int N_PLAYERS = 4;
	public enum State {
		STATE_HIDE_COINS, STATE_BID, STATE_SHOW_RESULT, STATE_END_GAME;
		
		public State next() {
			return values()[(ordinal()+1)%4];
		}
	};

	private ObjectProperty<State> state = new SimpleObjectProperty<State>(State.STATE_HIDE_COINS);
	
	private Player[] players = new Player[N_PLAYERS];
	
	public Game() {
		for (int i=0; i<N_PLAYERS; i++) {
			players[i] = new Player();
		}
	}
	
	public State getState() {
		return state.get();
	}
	
	public ObjectProperty<State> getStateProperty() {
		return state;
	}
	
	public void nextState() {
		state.set(getState().next());
	}
	
	public void setHiddenCoins(int hiddenCoins) {
		players[0].setHiddenCoins(hiddenCoins);
		for (int i=1; i<players.length; i++) {
			players[i].hide();
		}
	}
	
	public int getHiddenCoinsCount() {
		int count = 0;

		for (Player player : players){
			count += player.getHiddenCoins();
		}
		
		return count;
	}
	
	public Player getPlayer(int nPlayer) {
		return players[nPlayer];
	}
	
	public int getGuanyador() {
		// Total de monedes amagades
		int count = getHiddenCoinsCount();
		
		// Determinem qui ha guanyat
		int winnerPos=0;
		
		for (Player player : players){
			if (count == winnerPos)
				return winnerPos;
			winnerPos++;
		}
		
		return -1;
	}
	
	public void setBet(int bet) {
		players[0].setBet(bet);
		for (int i=1; i<players.length; i++)
			players[i].bid();
	}
}
