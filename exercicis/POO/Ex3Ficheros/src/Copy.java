import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Copy {

	public static void copiarFitxer(Path directorio, Path fitxer){
		try {
			Files.copy(fitxer, directorio,StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		if(args.length == 2){
			Path fitxer = Paths.get(args[0]);
			Path dir = Paths.get(args[1]);
			
			try{
				copiarFitxer(dir, fitxer);
			} catch(DirectoryIteratorException ex){
				System.err.println(ex);
			}
		}
	}
}