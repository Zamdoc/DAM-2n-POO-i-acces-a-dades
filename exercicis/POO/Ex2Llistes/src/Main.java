import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
	/* Sin querer lo he hecho de strings en vez de ints (Confusi�n de nombres con n�meros), pero as� de paso practico el compareTo*/
	public static boolean llistaord(List<String> nombres){
		boolean compr = true;
		boolean firsttime = false;
		int comprobacio;
		Iterator<String> itr = nombres.iterator();
		String nombre;
		String buffer="";
		
		while(itr.hasNext() && compr){
			nombre = itr.next();
			
			if(firsttime == false)
				firsttime = true;
			else{
				comprobacio = buffer.compareTo(nombre);
				if (comprobacio > 0)
					compr = false;
			}
			buffer = nombre;
		}
		return compr;
	}
	
	public static void main(String[] args) {
		List<String> cadenes = new ArrayList<String>();
		
		cadenes.add("Hola");
		cadenes.add("Adios");
		cadenes.add("Robomongo");
		cadenes.add("Hacker");
		cadenes.add("Andres");
		cadenes.add("Juan");
		cadenes.add("Hp");
		//Collections.sort(cadenes);
		boolean compr = llistaord(cadenes);
		if (compr == true)
			System.out.println("Llista ordenada");
		else
			System.out.println("Llista no ordenada");
	}
}
