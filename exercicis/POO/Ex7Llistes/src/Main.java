import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

	public static void main(String[] args) {
		LlistaOrdenada lo = new LlistaOrdenada();
		Scanner in = new Scanner(System.in);
		for (int i=0; i<=1000; i++)
			lo.afegirNumero(ThreadLocalRandom.current().nextInt(1, 100000 + 1));

		
		lo.mostraLlista();
		
		System.out.println("Escriu el numero a trobar: ");
		int numero = in.nextInt();
		
		lo.cercaNumero(numero);
		
		in.close();
		
		
		/*
		*El m�tode sort, per ordenar una llista, no est� incl�s a la interf�cie List ni a les
		*classes LinkedList o ArrayList, sin� que s'ha implementat com a m�tode est�tic de la classe Collections.
		*Per qu� creus que s'ha dissenyat d'aquesta manera?
		*
		*Perqu�, com que totes les llistes son comparables amb elles mateixes, i no hi ha cap necessitat de concretar
		*per ordenar elements d'un array (Que no tenen a veure amb la llista), pots comparar-los per ordenar-los
		*sense importar quin tipus de llista sigui.
		*
		*
		*Creus que hi pot haver algun problema amb la cerca bin�ria si es modifiquen els valors
		*dels atributs dels objectes que hi ha guardats a la llista? Llegeix la introducci� de la 
		*interf�cie Set a la documentaci� de les API i fixa't en la nota que hi ha sobre elements mutables.
		*Per qu� creus que hi ha aquesta advert�ncia?
		*
 		* Si, si es modifiquen valors en una llista i no es torna a ordenar, binarySearch donar� errors al trobar elements
 		* si coincideix amb un element no ordenat quan busqui.
 		* L'advertencia hi es perqu� probablement estigui implementada utilitzant l'equals 
 		* i aix� doni conflictes al utilitzar-lo si canvies elements.
 		*/
	}
}
