import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class LlistaOrdenada {
	
	private List<Integer> llista = new ArrayList<Integer>();
	ListIterator<Integer> itr;
	Scanner in = new Scanner(System.in);
	
	/*
	 * Afegeix un n�mero a la llista de la classe, ja ordenat dins de la seva posici� de menor a major.
	 * 
	 * @param: integer
	 */
	public void afegirNumero (int numero){
		int posicio;
		posicio = cercaNumero(numero);
		posicio++;
		posicio = Math.abs(posicio);
		System.out.println("Es colocar� a la posici�: "+posicio);
		llista.add(posicio, numero);
	}
	
	/*
	 * Esborra un n�mero de la llista. Et dona una llista amb el seu �ndex, i es selecciona 
	 * l'�ndex del n�mero a esborrar.
	 */
	public void eliminarNumero (){
		itr = llista.listIterator();
		int response;
		
		mostraLlista();
		System.out.println("Escull el numero (index) a eliminar: ");
		response=in.nextInt();
			
		System.out.println("Numero a l'index "+response+" eliminat.");
		llista.remove(response);

	}
	
	
	/*
	 * Busca de manera bin�ria un n�mero.
	 */
	public int cercaNumero(int numero){
		
		return Collections.binarySearch(llista, numero);
		
//		boolean found = false;
//		int low = 0;
//	    int high = llista.size() - 1;
//	    int mid = 0;
//	    while (low <= high && found == false) {
//	        mid = (low + high) / 2;
//	        if (llista.get(mid) > numero) {
//	            high = mid - 1;
//	            
//	        } else if (llista.get(mid) < numero) {
//	            low = mid + 1;
//	        } else {
//	        	found = true;
//	            System.out.println("Element trobat a la posici� "+mid);
//	            return mid;
//	        }
//	    }
//	    
//		if (!found)
//			System.out.println("Element no trobat");
//		if (low>mid)
//			mid=low;
//		
//		return mid-mid*2;
	}
	
	
	/*
	 * Simple m�tode per mostrar tota la llista
	 */
	public void mostraLlista(){
		itr = llista.listIterator();
		
		while(itr.hasNext())
			System.out.println("Numero "+itr.nextIndex()+": "+itr.next());
	}
}
