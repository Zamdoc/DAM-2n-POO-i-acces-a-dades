import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Paisos {

	public static void main(String[] args) {
		
		Map<String, String> llistapaisos = new HashMap<String, String>();
		Scanner in = new Scanner(System.in);
		String input="not empty";
		String inputcapital="";
		boolean trobat;
		llistapaisos.put("Espanya", "Madrid");
		llistapaisos.put("Fran�a", "Par�s");
		llistapaisos.put("It�lia", "Roma");
		llistapaisos.put("Anglaterra", "Londres");
		llistapaisos.put("Alemanya", "Berl�n");
		llistapaisos.put("Prova", "");
		
		do{
			System.out.print("Entra capital: ");
			trobat = false;
			input = in.nextLine();
			Set<String> claus = llistapaisos.keySet();
			for (String clau : claus)
				if(clau.equals(input) && !llistapaisos.get(clau).isEmpty()){
					System.out.println("La capital es: "+llistapaisos.get(clau));
					trobat = true;
				}
			
			if (!trobat){
				System.out.println("No s'ha trobat la capital, si vols, pots afegir-la: ");
				inputcapital = in.nextLine();
			}
			
			if (!inputcapital.isEmpty())
				llistapaisos.put(input, inputcapital);
			
		}while (!input.isEmpty());
		
	}

}
