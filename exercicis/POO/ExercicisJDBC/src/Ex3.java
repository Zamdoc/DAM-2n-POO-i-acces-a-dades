import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex3 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT rental.rental_date, film.title, inventory.store_id FROM customer "
					+ "JOIN rental ON customer.customer_id = rental.customer_id "
					+ "JOIN inventory ON rental.inventory_id = inventory.inventory_id "
					+ "JOIN film ON inventory.film_id = film.film_id "
					+ "WHERE customer.first_name = 'ALLISON' AND last_name='STANLEY'")){
				System.out.println("Lloguer ALLISON STANLEY: ");
				while (rs.next()) {
					String name = rs.getString("film.title");
					String storeid = rs.getString("inventory.store_id");
					Date date = rs.getDate("rental.rental_date");
					System.out.println("Nom: " + name + " ");
					System.out.println("ID tenda: " + storeid);
					System.out.println("Data venciment: " + date);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}

}
