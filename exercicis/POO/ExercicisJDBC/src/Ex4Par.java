import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Ex4Par {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");

		String pelicula = " ";
		String titol = " ";
		try (Connection con = DriverManager.getConnection(url, connectionProperties); Scanner in = new Scanner(System.in)) {
			
			PreparedStatement st = con.prepareStatement("SELECT DISTINCT title, store.store_id FROM film "
					+ "JOIN inventory ON film.film_id = inventory.film_id "
					+ "JOIN store ON inventory.store_id = store.store_id "
					+ "WHERE title LIKE ? OR description LIKE ? ORDER BY title");
			
			while (!pelicula.equals("")) {
				System.out.print("Paraula clau a buscar: ");
				pelicula = in.nextLine();
				if (!pelicula.equals("")) {
					st.setString(1, "%" + pelicula + "%");
					st.setString(2, "%" + pelicula + "%");
					System.out.println("Pel�licules amb la paraula clau \"" + pelicula + "\": ");

					try (ResultSet rs = st.executeQuery()) {
						while (rs.next()) {
							if (!titol.equals(rs.getString("title"))) {
								titol = rs.getString("title");
								System.out.println(titol + ": ");
							}
							int storeId = rs.getInt("store_id");
							System.out.println("\tBotiga " + storeId);
						}
					}
				}
				System.out.println();
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}