import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex6 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT category.name, count(category.name) FROM category "
					+ "JOIN film_category on (category.category_id = film_category.category_id) GROUP BY cat.name")){
				System.out.println("Categories: ");
				while (rs.next()) {
					String categoria = rs.getString("category.name");
					int quantitat = rs.getInt("count(category.name)");
					System.out.println("Categoria: " + categoria);
					System.out.println("Quantitat: " + quantitat);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}
