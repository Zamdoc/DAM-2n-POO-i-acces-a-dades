import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class Ex1Sent {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/onomastica";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		String file = "D:\\Kevin\\Noms_Nascuts_2012.csv";
		long startTime;
		long endTime;
		long duration;
		int opcio = 0;
		
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Scanner in = new Scanner(System.in)) {
			PreparedStatement erase = con.prepareStatement("TRUNCATE TABLE noms2012");
			
			do {
				System.out.println("=========================================");
				System.out.println("Opci� 1. Fent servir un objecte Statement. ");
				System.out.println("Opci� 2. Fent servir un objecte PreparedStatement. ");
				System.out.println("Opci� 3. Inserint tots els valors en una �nica consulta SQL. ");
				System.out.println("Opci� 4. Borrar base de dades. ");
				System.out.println("0. Sortir. ");
				System.out.println("=========================================");
				System.out.print("Opci�: ");

				opcio = in.nextInt();
				in.nextLine();

				switch (opcio) {

				case 1:
					try(BufferedReader br = new BufferedReader(new FileReader(file))){
						startTime = System.nanoTime();
						System.out.println("Opci� 1. Fent servir un objecte Statement. ");
						Statement st = con.createStatement();
						erase.executeUpdate();
					    for(String line; (line = br.readLine()) != null; ) {
					    	String[] lineaseparada = line.split(";");
					    	System.out.println(lineaseparada[0]);
					    	lineaseparada[4] = lineaseparada[4].replaceAll(",",".");
					    	st.executeUpdate("INSERT INTO noms2012 VALUES ("+lineaseparada[0]+",'"+lineaseparada[1]+"','"
					    	+lineaseparada[2]+"',"+lineaseparada[3]+","+lineaseparada[4]+")");
					    }
					    endTime = System.nanoTime();
					    duration = (endTime - startTime) / 1000000;
					    System.out.println("Temps d'execuci�: "+ duration +" milisegons");
					}
					break;
				case 2:
					try(BufferedReader br = new BufferedReader(new FileReader(file))){
						startTime = System.nanoTime();
						System.out.println("Opci� 2. Fent servir un objecte PreparedStatement. ");
						erase.executeUpdate();
						PreparedStatement pst = con.prepareStatement("INSERT INTO noms2012 VALUES (?,?,?,?,?)");
					    for(String line; (line = br.readLine()) != null; ) {
					    	String[] lineaseparada = line.split(";");
					    	pst.setString(1, lineaseparada[0]);
					    	System.out.println(lineaseparada[0]);
					    	pst.setString(2, lineaseparada[1]);
					    	pst.setString(3, lineaseparada[2]);
					    	pst.setString(4, lineaseparada[3]);
					    	lineaseparada[4] = lineaseparada[4].replaceAll(",",".");
					    	pst.setString(5, lineaseparada[4]);
					    	pst.executeUpdate();
					    }
					    endTime = System.nanoTime();
					    duration = (endTime - startTime) / 1000000;
					    System.out.println("Temps d'execuci�: "+ duration +" milisegons");
					}
					break;
				case 3:
					try(BufferedReader br = new BufferedReader(new FileReader(file))){
						startTime = System.nanoTime();
						System.out.println("Opci� 3. Inserint tots els valors en una �nica consulta SQL. ");
						
						Statement st2 = con.createStatement();
						erase.executeUpdate();
						StringBuilder sb = new StringBuilder();
						
					    for(String line; (line = br.readLine()) != null; ) {
					    	String[] lineaseparada = line.split(";");
					    	lineaseparada[4] = lineaseparada[4].replaceAll(",",".");
					    	sb.append("(");
					    	sb.append(lineaseparada[0]+",'"+lineaseparada[1]+"','"
					    	+lineaseparada[2]+"',"+lineaseparada[3]+","+lineaseparada[4]);
					    	sb.append("), ");
					    }
					    sb.setLength(sb.length()-2);
					    st2.executeUpdate("INSERT INTO noms2012 VALUES "+sb.toString());
					    endTime = System.nanoTime();
					    duration = (endTime - startTime) / 1000000;
					    System.out.println("Temps d'execuci�: "+ duration +" milisegons");
					}
					break;
				case 4:
					System.out.println("Opci� 4. Borrar base de dades. ");
					erase.executeUpdate();
					break;
				case 0:
					System.out.println("Has escollit sortir.");
					break;
				default:
					System.out.println("Torna a provar. ");
					break;
				}
			} while (opcio != 0);
			
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
