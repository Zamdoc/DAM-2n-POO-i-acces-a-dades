import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex1 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT * FROM film where length >= 150")) {
				System.out.println("Pel�licules amb una durada m�s gran que 2:30 hores: ");
				while (rs.next()) {
					String name = rs.getString("title");
					String description = rs.getString("description");
					System.out.print("Nom: " + name + " ");
					System.out.println("Descripci� :" + description);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}

}
