import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex5 {
	
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT customer.first_name, customer.last_name, address.phone, film.title FROM customer "
					+ "JOIN address ON customer.address_id = address.address_id "
					+ "JOIN rental ON customer.customer_id = rental.customer_id "
					+ "JOIN inventory ON rental.inventory_id = inventory.inventory_id "
					+ "JOIN film ON inventory.film_id = film.film_id "
					+ "WHERE CURDATE() > DATE_ADD(rental.rental_date,INTERVAL film.rental_duration DAY) "
					+ "AND rental.return_date is null")){
				System.out.println("Botigues: ");
				while (rs.next()) {
					String name = rs.getString("customer.first_name");
					String lastname = rs.getString("customer.last_name");
					String phone = rs.getString("address.phone");
					String titol = rs.getString("film.title");
					System.out.println("Nom: " + name + " ");
					System.out.println("Cognom: " + lastname);
					System.out.println("Phone: " + phone);
					System.out.println("Title: " + titol);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}

}
