import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Ex2Par {
	
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		String nombreCustomer;
		String apellidoCustomer;
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Scanner in = new Scanner(System.in)) {
			
			System.out.println("Base de dades connectada!");
			System.out.println("Escriu el nom del customer: ");
			nombreCustomer = in.nextLine();
			System.out.println("Escriu el cognom del customer: ");
			apellidoCustomer = in.nextLine();
			
			
			PreparedStatement st = con.prepareStatement("SELECT rental.rental_date, film.title, inventory.store_id FROM customer "
					+ "JOIN rental ON customer.customer_id = rental.customer_id "
					+ "JOIN inventory ON rental.inventory_id = inventory.inventory_id "
					+ "JOIN film ON inventory.film_id = film.film_id "
					+ "WHERE customer.first_name = ? AND last_name = ?");
			if (!nombreCustomer.equals("") && !apellidoCustomer.equals("")) {
                st.setString(1, nombreCustomer);
                st.setString(2, apellidoCustomer);
			}
			
			try (ResultSet rs = st.executeQuery()){
				System.out.println("Lloguer: ");
				while (rs.next()) {
					String name = rs.getString("film.title");
					String storeid = rs.getString("inventory.store_id");
					Date date = rs.getDate("rental.rental_date");
					System.out.println("Nom: " + name + " ");
					System.out.println("ID tenda: " + storeid);
					System.out.println("Data venciment: " + date);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}
