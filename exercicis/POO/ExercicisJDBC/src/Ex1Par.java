import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;


public class Ex1Par {
	
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String apellidoActor;
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Scanner in = new Scanner(System.in)) {
			System.out.println("Base de dades connectada!");
			System.out.println("Escriu el cognom del actor: ");
			apellidoActor = in.nextLine();
			
			PreparedStatement st = con.prepareStatement("SELECT film.title FROM film "
					+ "JOIN film_actor ON film.film_id = film_actor.film_id "
					+ "JOIN actor ON film_actor.actor_id = actor.actor_id "
					+ "WHERE actor.last_name LIKE ?");
			if (!apellidoActor.equals("")) {
                st.setString(1, apellidoActor);
			}
			try (ResultSet rs = st.executeQuery()) {
				System.out.println("Pel�l�cules de: " + apellidoActor);
				
				while (rs.next()) {
					String title = rs.getString("title");
					System.out.println("T�tol: " + title);
				}
				
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}
