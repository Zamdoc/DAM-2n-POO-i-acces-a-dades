import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex1Meta {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			
			DatabaseMetaData dbmd = null;
			dbmd = con.getMetaData();
			ResultSet rs;
			
			rs = dbmd.getPrimaryKeys(null, null, "actor");
			
			while (rs.next()){
				System.out.println("Clau primaria de la taula \"actor\": " + rs.getString("COLUMN_NAME"));
			}
			
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}