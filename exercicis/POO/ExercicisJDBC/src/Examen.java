import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Examen {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String usuari;
		int id = 0;
		String nom = "";
		do {
			System.out.println("Introduce un nombre de usuario: ");
			usuari = in.nextLine();
			String url = "jdbc:mysql://localhost:3306/sakila";
			Properties connectionProperties = new Properties();
			connectionProperties.setProperty("user", "root");

			try (Connection con = DriverManager.getConnection(url, connectionProperties)) {
				PreparedStatement st = con
						.prepareStatement("SELECT staff_id, first_name, last_name FROM staff where username = ?");
				if (!usuari.equals("")) {
					st.setString(1, usuari);
				}
				try (ResultSet rs = st.executeQuery()) {

					while (rs.next()) {
						id = rs.getInt("staff_id");
						nom = rs.getString("first_name");
						String cognom = rs.getString("last_name");
						System.out.println("Id de l'empleat: " + id);
						System.out.println("Nom de l'empleat: " + nom);
						System.out.println("Cognom de l'empleat: " + cognom);
					}

				}
			} catch (SQLException e) {
				System.err.println("Error d'establiment de connexi�: " + e.getMessage());
			}

			if (nom.equals(""))
				System.out.println("Usuario inv�lido, vuelve a intentarlo");
			else {
				int opcio;
				do {

					System.out.println("=========================================");
					System.out.println("Opci� 1. Llogar pel�l�cula. ");
					System.out.println("Opci� 2. Retornar pel�l�cula. ");
					System.out.println("Opci� 3. Sortir del programa. ");
					System.out.println("=========================================");
					System.out.print("Opci�: ");
					opcio = in.nextInt();
					in.nextLine();

					switch (opcio) {

					case 1:

						System.out.println("Introdueix el id o el cognom del client: ");
						String resultat = in.nextLine();

						try (Connection con = DriverManager.getConnection(url, connectionProperties)) {
							List<Integer> listaclients = new ArrayList<Integer>();
							int seleccio = 0;
							try {
								Double resultatid = Double.parseDouble(resultat);

								PreparedStatement st = con
										.prepareStatement("SELECT * FROM customer where customer_id = ?");
								st.setString(1, resultat);

								try (ResultSet rs = st.executeQuery()) {

									while (rs.next()) {
										Integer idclient = rs.getInt("customer_id");
										System.out.println("Id del client: " + idclient);
										String cognom = rs.getString("last_name");
										listaclients.add(idclient);
									}

								}

							} catch (NumberFormatException nfe) {
								PreparedStatement st = con
										.prepareStatement("SELECT * FROM customer where last_name LIKE ?");
								st.setString(1, resultat);
								try (ResultSet rs = st.executeQuery()) {

									while (rs.next()) {
										int idclient = rs.getInt("customer_id");
										listaclients.add(idclient);
										String cognom = rs.getString("last_name");
										System.out.println("Cognom del client 1: " + cognom);
									}
									System.out.println("Selecciona un client: ");
									seleccio = in.nextInt();
									in.nextLine();
									if (listaclients.size() > seleccio - 1) {
										int buffer = listaclients.get(seleccio - 1);
										listaclients.clear();
										listaclients.add(buffer);
									} else
										System.out.println("Numero invalid, sortint...");
								}
							}
							Integer client = 0;
							if (listaclients.size() > 0) {
								client = listaclients.get(0);
								llogar(con, client, id);
							}

						} catch (SQLException e) {
							System.err.println("Error d'establiment de connexi�: " + e.getMessage());
						}

						break;
					case 2:

						System.out.println("Inserta id del item a tornar: ");
						int item = in.nextInt();
						in.nextLine();
						try (Connection con = DriverManager.getConnection(url, connectionProperties)) {

							PreparedStatement st = con.prepareStatement(
									"UPDATE rental SET return_date = ? WHERE rental.inventory_id = ?");

							java.util.Date dt = new java.util.Date();

							java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

							String currentTime = sdf.format(dt);

							st.setString(1, currentTime);
							st.setInt(2, item);

							st.executeUpdate();

						} catch (SQLException e) {
							System.err.println("Error d'establiment de connexi�: " + e.getMessage());
						}
						break;
					case 3:

						break;
					}
				} while (opcio != 3);

			}

		} while (nom.equals(""));

		in.close();
	}

	public static void llogar(Connection con, Integer clientid, Integer staffid) {
		int peliculaid = 1;

		try {
			con.setAutoCommit(false);
			PreparedStatement st = con.prepareStatement(
					"INSERT INTO rental (rental_date, inventory_id, customer_id, staff_id, last_update) VALUES (?,?,?,?,?)");

			java.util.Date dt = new java.util.Date();

			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String currentTime = sdf.format(dt);
			st.setString(1, currentTime);
			st.setInt(2, peliculaid);
			st.setInt(3, clientid);
			st.setInt(4, staffid);
			st.setString(5, currentTime);

			st.executeUpdate();

			st = con.prepareStatement(
					"INSERT INTO payment (customer_id, staff_id, amount, payment_date, last_update) VALUES (?,?,?,?,?)");

			PreparedStatement st2 = con.prepareStatement("SELECT film.rental_rate FROM film "
					+ "JOIN inventory ON film.film_id = inventory.film_id " + "WHERE inventory_id = ?");

			st2.setInt(1, peliculaid);
			Double precio = 0.00;
			try (ResultSet rs = st2.executeQuery()) {

				while (rs.next()) {
					precio = rs.getDouble("film.rental_rate");
				}

			}

			st.setInt(1, clientid);
			st.setInt(2, staffid);
			st.setDouble(3, precio);
			st.setString(4, currentTime);
			st.setString(5, currentTime);

			st.executeUpdate();

			con.commit();

			System.out.println("Pel�l�cula llogada.");
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}
}
