import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex4 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT store.store_id, address.address, first_name, last_name FROM store "
					+ "JOIN staff ON store.manager_staff_id = staff.staff_id "
					+ "JOIN address ON staff.address_id = address.address_id")){
				System.out.println("Botigues: ");
				while (rs.next()) {
					String name = rs.getString("first_name");
					String lastname = rs.getString("last_name");
					String storeid = rs.getString("store.store_id");
					String address = rs.getString("address.address");
					System.out.println("Nom: " + name + " ");
					System.out.println("Cognom: " + lastname);
					System.out.println("Store ID: " + storeid);
					System.out.println("Direcci�: " + address);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}

}
