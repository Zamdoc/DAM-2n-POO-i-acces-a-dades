import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Ex2 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT first_name, last_name FROM film "
					+ "JOIN film_actor ON film.film_id = film_actor.film_id "
					+ "JOIN actor ON film_actor.actor_id = actor.actor_id WHERE title = 'TWISTED PIRATES'")){
				System.out.println("Actors: ");
				while (rs.next()) {
					String name = rs.getString("first_name");
					String description = rs.getString("last_name");
					System.out.println("Nom: " + name + " ");
					System.out.println("Cognom: " + description);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}
