import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex5Par {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		int botiga;
		int cerca;
		int dades;

		Ex5ParBBDD bbdd = new Ex5ParBBDD();

		List<String> pelicules = new ArrayList<String>();

		int maximBotigues = bbdd.mostraBotigues();

		do {
			System.out.print("\nSelecciona una botiga: ");
			botiga = sc.nextInt();
			sc.nextLine();

		} while (botiga > maximBotigues || botiga <= 0);

		do {
			System.out.println("=========================================");
			System.out.println("Opci� 1. Buscar per t�tol. ");
			System.out.println("Opci� 2. Buscar per nom d'actor. ");
			System.out.println("Opci� 3. Buscar per g�nere de pel�l�cula. ");
			System.out.println("0. Sortir. ");
			System.out.println("=========================================");
			System.out.print("Opci�: ");

			cerca = sc.nextInt();

			sc.nextLine();

			switch (cerca) {

			case 1:
				String titol;
				do {
					System.out.print("Introdueix el t�tol a cercar: ");
					titol = sc.nextLine();
				} while (titol.equals(""));

				pelicules = bbdd.cercaTitol(botiga, titol);

				if (pelicules.size() > 0) {
					System.out.println("\t" + (pelicules.size() + 1) + ". Sortir. ");

					do {
						System.out.print("Introdueix el n�mero d'una pel�l�cula per veuren les dades: ");
						dades = sc.nextInt();
						sc.nextLine();
					} while (dades > pelicules.size() + 1 || dades <= 0);

					bbdd.dadesPeli(pelicules.get(dades - 1));
				}
				break;

			case 2:
				String nom;
				String cognom;
				do {
					System.out.print("Introdueix el nom de l'actor: ");
					nom = sc.nextLine();
					System.out.print("Introdueix el cognom de l'actor: ");
					cognom = sc.nextLine();
				} while (nom.equals("") || cognom.equals(""));

				pelicules = bbdd.cercaActor(botiga, nom, cognom);

				if (pelicules.size() > 0) {
					System.out.println("\t" + (pelicules.size() + 1) + ". Tornar al men� principal. ");
					do {
						System.out.print("Introdueix el n�mero d'una pel�l�cula per veuren les dades: ");
						dades = sc.nextInt();
						sc.nextLine();
					} while (dades > pelicules.size() + 1 || dades <= 0);
					bbdd.dadesPeli(pelicules.get(dades - 1));
				}
				break;

			case 3:
				String categoria;
				do {
					System.out.print("Introdueix el g�nere: ");
					categoria = sc.nextLine();
				} while (categoria.equals(""));
				pelicules = bbdd.cercaGenere(botiga, categoria);

				if (pelicules.size() > 0) {
					System.out.println("\t" + (pelicules.size() + 1) + ". Tornar al men� principal. ");

					do {
						System.out.print("Introdueix el n�mero d'una pel�l�cula per veuren les dades: ");
						dades = sc.nextInt();
						sc.nextLine();
					} while (dades > pelicules.size() + 1 || dades <= 0);

					bbdd.dadesPeli(pelicules.get(dades - 1));
				}
				break;
			case 0:
				System.out.println("Has escollit sortir.");
				break;
			default:
				System.out.println("Torna a provar. ");
				break;
			}

		} while (cerca != 0);
		sc.close();
	}
}