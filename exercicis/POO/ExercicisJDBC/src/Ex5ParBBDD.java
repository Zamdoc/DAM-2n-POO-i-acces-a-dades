import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Ex5ParBBDD {

	private String url;
	private Properties connectionProperties;

	public Ex5ParBBDD() {
		url = "jdbc:mariadb://localhost:3306/sakila";
		connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
	}

	public int mostraBotigues() {
		int i = 0;
		try (Connection con = DriverManager.getConnection(url, connectionProperties)) {
			
			System.out.println("Botigues: ");

			PreparedStatement st = con.prepareStatement("SELECT store_id, address FROM store "
					+ "JOIN address ON store.address_id = address.address_id");
			try (ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					int storeId = rs.getInt("store_id");
					String address = rs.getString("address");
					System.out.println("\tBotiga " + storeId + ". Adre�a: " + address + ". ");
					i++;
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}

		return i;
	}

	public List<String> cercaTitol(int botiga, String titol) {

		int i = 0;
		boolean compr;
		List<String> pelis = new ArrayList<String>();
		
		try (Connection con = DriverManager.getConnection(url, connectionProperties)){

				PreparedStatement st = con.prepareStatement(
								"SELECT title, store.store_id, rental.return_date, date_add(rental_date, interval rental_duration day) AS data_retorn FROM film "
								+ "JOIN inventory ON film.film_id = inventory.film_id "
								+ "JOIN store ON inventory.store_id = store.store_id "
								+ "JOIN rental ON inventory.inventory_id = rental.inventory_id "
								+ "WHERE store.store_id = ? AND title LIKE ? " + "ORDER BY title");

			System.out.println("Pel�l�cula: " + titol + "  Botiga:  " + botiga);

			st.setInt(1, botiga);
			st.setString(2, "%" + titol + "%");
			
			try (ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					compr = false;
					String title = rs.getString("title");

					Date returnDate = rs.getDate("return_date");

					if (returnDate == null)
						compr = true;

					LocalDate dataRetorn = rs.getDate("data_retorn").toLocalDate();
					System.out.print("\t" + (i + 1) + ". " + title);

					if (compr == false)
						System.out.println(". DISPONIBLE. ");
					else
						System.out.println(". NO DISPONIBLE. Data de retorn: " + dataRetorn + ". ");
					
					i++;
					pelis.add(title);
				}

				if (i == 0)
					System.out.println("No existeix aquesta pel�l�cula. ");
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}

		return pelis;
	}

	public List<String> cercaActor(int botiga, String nom, String cognom) {

		int i = 0;
		boolean compr;
		List<String> pelicules = new ArrayList<String>();

		try (Connection con = DriverManager.getConnection(url, connectionProperties)){

				PreparedStatement st = con.prepareStatement(
								"SELECT title, store.store_id, rental.return_date, date_add(rental_date, interval rental_duration day) AS data_retorn FROM film "
								+ "JOIN inventory ON film.film_id = inventory.film_id "
								+ "JOIN store ON inventory.store_id = store.store_id "
								+ "JOIN rental ON inventory.inventory_id = rental.inventory_id "
								+ "JOIN film_actor ON film.film_id = film_actor.film_id "
								+ "JOIN actor ON film_actor.actor_id = actor.actor_id "
								+ "WHERE store.store_id = ? AND first_name LIKE ? AND last_name LIKE ? "
								+ "ORDER BY title");

			System.out.println("Pel�l�cules del actor: " + nom + " " + cognom + " De la botiga: " + botiga);

			st.setInt(1, botiga);
			st.setString(2, "%" + nom + "%");
			st.setString(3, "%" + cognom + "%");

			try (ResultSet rs = st.executeQuery()) {
				while (rs.next()) {

					compr = false;
					String titol = rs.getString("title");
					Date returnDate = rs.getDate("return_date");
					
					if (returnDate == null)
						compr = true;

					LocalDate dataRetorn = rs.getDate("data_retorn").toLocalDate();
					System.out.print("\t" + (i + 1) + ". " + titol);

					if (compr == false)
						System.out.println(". DISPONIBLE. ");
					else
						System.out.println(". NO DISPONIBLE. Data de retorn: " + dataRetorn + ". ");

					i++;
					pelicules.add(titol);
				}

				if (i == 0)
					System.out.println("No existeix cap pel�l�cula amb aquest actor. ");

			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}

		return pelicules;
	}

	public List<String> cercaGenere(int botiga, String categoria) {

		int i = 0;

		boolean check;

		List<String> pelis = new ArrayList<String>();

		try (Connection con = DriverManager.getConnection(url, connectionProperties);

				PreparedStatement st = con.prepareStatement(

						"SELECT title, store.store_id, rental.return_date, date_add(rental_date, interval rental_duration day) AS data_retorn FROM film "
								+ "JOIN inventory ON film.film_id = inventory.film_id "
								+ "JOIN store ON inventory.store_id = store.store_id "
								+ "JOIN rental ON inventory.inventory_id = rental.inventory_id "
								+ "JOIN film_category ON film.film_id = film_category.film_id "
								+ "JOIN category ON film_category.category_id = category.category_id "
								+ "WHERE store.store_id = ? AND category.name LIKE ? " + "ORDER BY title")) {

			System.out.println("Pel�l�cules del g�nere: " + categoria + " De la botiga: " + botiga);

			st.setInt(1, botiga);
			st.setString(2, "%" + categoria + "%");

			try (ResultSet rs = st.executeQuery()) {

				while (rs.next()) {
					check = false;
					String title = rs.getString("title");
					Date returnDate = rs.getDate("return_date");

					if (returnDate == null)
						check = true;

					LocalDate dataRetorn = rs.getDate("data_retorn").toLocalDate();
					System.out.print("\t" + (i + 1) + ". " + title);

					if (check == false)
						System.out.println(". DISPONIBLE. ");
					else
						System.out.println(". NO DISPONIBLE. Data de retorn: " + dataRetorn + ". ");
					
					i++;
					pelis.add(title);
				}

				if (i == 0)
					System.out.println("No tenim cap pel�l�cula d'aquest g�nere. ");
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}

		return pelis;
	}

	public void dadesPeli(String titol) {

		String dades;

		try (Connection con = DriverManager.getConnection(url, connectionProperties)){

				PreparedStatement st = con.prepareStatement(
						"SELECT film_id, title, description, release_year FROM film " + "WHERE title LIKE ?");
				PreparedStatement st2 = con.prepareStatement("SELECT first_name, last_name FROM actor "
						+ "JOIN film_actor ON actor.actor_id = film_actor.actor_id " + "WHERE film_id = ?");

			System.out.println("Dades de la pel�l�cula: " + titol);

			st.setString(1, "%" + titol + "%");

			try (ResultSet rs = st.executeQuery()) {

				while (rs.next()) {
					
					int id = rs.getInt("film_id");
					String title = rs.getString("title");
					String description = rs.getString("description");
					int releaseYear = rs.getInt("release_year");

					dades = "T�tol: " + title + "Descripci�: " + description + "Any de producci�: " + releaseYear + "Actors: ";

					st2.setInt(1, id);

					try (ResultSet rs2 = st2.executeQuery()) {

						while (rs2.next()) {

							String nom = rs2.getString("first_name");
							String cognom = rs2.getString("last_name");

							dades += nom + " " + cognom + "\n";
						}
					}
					System.out.print(dades);
				}
			}

		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}