import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Ex3Par {
	
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		String nombreClient;
		String startDate;
		String endDate;
		try (Connection con = DriverManager.getConnection(url, connectionProperties);
				Scanner in = new Scanner(System.in)) {
			
			System.out.println("Base de dades connectada!");
			System.out.println("Escriu el nom del client: ");
			nombreClient = in.nextLine();
			System.out.println("Escriu la data inicial: (Exemple: 2010-01-30 14:15:55)");
			startDate = in.nextLine();
			System.out.println("Escriu la data final: (Exemple: 2010-01-30 14:15:55)");
			endDate = in.nextLine();
			
			PreparedStatement st = con.prepareStatement("SELECT rental.rental_date, film.rental_rate, film.title FROM customer "
					+ "JOIN rental ON customer.customer_id = rental.customer_id "
					+ "JOIN inventory ON rental.inventory_id = inventory.inventory_id "
					+ "JOIN film ON inventory.film_id = film.film_id "
					+ "WHERE (rental.rental_date BETWEEN ? AND ? ) "
					+ "AND customer.first_name = ? ");
			
			if (!nombreClient.equals("") && !startDate.equals("") && !endDate.equals("")) {
                st.setString(1, nombreClient);
                st.setString(2, startDate);
                st.setString(3, endDate);
			}
			
			try (ResultSet rs = st.executeQuery()){
				while (rs.next()) {
					String titol = rs.getString("film.title");
					String pagament = rs.getString("film.rental_rate");
					Date date = rs.getDate("rental.rental_date");
					System.out.println("Titol: " + titol);
					System.out.println("Preu: " + pagament);
					System.out.println("Data: " + date);
				}
			}
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexi�: " + e.getMessage());
		}
	}
}
