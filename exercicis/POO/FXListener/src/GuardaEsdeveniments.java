import java.util.ArrayList;
import java.util.List;

public class GuardaEsdeveniments implements Listener{

	
	List<Integer> events = new ArrayList<Integer>(); 
	
	@Override
	public void notifyEvent(int event) {
		events.add(event);
	}
	
	public String toString(){
		StringBuilder buffer = new StringBuilder();
		
		for (int i = 0; i < events.size(); i++){
			buffer.append(events.get(i)).append(" ");
		}
		String output = buffer.toString();
		return output;
	}
}
