package view;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.Region;

import model.Owner;

public class BoardSquare extends Region {
	private ObjectProperty<Owner> ownerProperty = new SimpleObjectProperty<>(Owner.NONE);
	
	public BoardSquare() {
		setPrefSize(50, 50);
		setMaxSize(50, 50);
		styleProperty().bind(Bindings.when(ownerProperty.isEqualTo(Owner.NONE))
				.then("-fx-background-color: #000000;")
				.otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUE))
						.then("-fx-background-color: #0000ff;")
						.otherwise("-fx-background-color: #00ff00;"))
				.concat("-fx-border-color: yellow"));
	}
	
	public ObjectProperty<Owner> getOwnerProperty() {
		return ownerProperty;
	}
	
	public Owner getOwner() {
		return ownerProperty.get();
	}
	
	public void setOwner(Owner owner) {
		ownerProperty.set(owner);
	}
}