package view;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import model.Game;
import model.Owner;

public class FourInARow extends Application implements EventHandler<ActionEvent> {
	private Button newGameButton = new Button("Nova partida");
	private BoardSquare[][] boardSquares = new BoardSquare[Game.HEIGHT][Game.WIDTH];
	private PlayButton[] playButtons = new PlayButton[Game.WIDTH];
	private Label infoLabel;
	private BoardSquare turnSquare = new BoardSquare();
	
	private Game game = new Game();

	public static void main(String[] args) {
		launch(args);
	}
	
	private Node createTopNode() {
		HBox topNode = new HBox();
		topNode.setPadding(new Insets(5, 5, 5, 5));
		topNode.getChildren().add(newGameButton);
		return topNode;
	}
	
	private Node createCenterNode() {
		GridPane centerNode = new GridPane();

		for (int i = 0; i < Game.HEIGHT; i++){
			for (int k = 0; k < Game.WIDTH; k++){
				boardSquares[i][k] = new BoardSquare();
				centerNode.add(boardSquares[i][k], k, i);
			}
		}
		
		for (int i = 0; i < Game.WIDTH; i++){
			playButtons[i] = new PlayButton(i);
			centerNode.add(playButtons[i], i, Game.HEIGHT);
		}
		return centerNode;
	}
	
	private Node createRightNode() {
		VBox rightNode = new VBox();
		infoLabel = new Label("Torn del jugador:");
		infoLabel.setPrefWidth(120);
		infoLabel.setAlignment(Pos.CENTER);
		rightNode.setPadding(new Insets(5, 5, 10, 10));
		rightNode.setSpacing(10);
		rightNode.getChildren().addAll(infoLabel, turnSquare);
		rightNode.setAlignment(Pos.TOP_CENTER);
		return rightNode;
	}
	
	private BorderPane createBorderPane() {
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(createTopNode());
		borderPane.setRight(createRightNode());
		borderPane.setCenter(createCenterNode());
		return borderPane;
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane root = createBorderPane();
		setBindings();
		setEventHandlers();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.setTitle("4 en ratlla");
		primaryStage.show();
	}

	private void setBindings() {
		turnSquare.getOwnerProperty().bind(
			Bindings.when(game.getWinnerProperty().isNotEqualTo(Owner.NONE))
			.then(game.getWinnerProperty())
			.otherwise(
				Bindings.when(game.getNTurnsProperty().greaterThanOrEqualTo(Game.HEIGHT*Game.WIDTH))
				.then(Owner.NONE)
				.otherwise(game.getTurnProperty())
			)
		);
		
		for (int i = 0; i < Game.HEIGHT; i++){
			for (int k = 0; k < Game.WIDTH; k++){
				boardSquares[i][k].getOwnerProperty().bind(game.getBoardProperty(i, k));
			}
			
		}
		
		for (int i = 0; i < Game.WIDTH; i++){
			playButtons[i].disableProperty().bind(Bindings.lessThan(game.getColFillsProperty(i), 0)
				.or(Bindings.notEqual(game.getWinnerProperty(), Owner.NONE)));
		}
		
		infoLabel.textProperty().bind(
				Bindings.when(game.getWinnerProperty().isNotEqualTo(Owner.NONE))
				.then("Victoria de: ")
				.otherwise(
						Bindings.when(game.getNTurnsProperty().greaterThanOrEqualTo(Game.HEIGHT*Game.WIDTH))
						.then("�s un empat!!")
						.otherwise("Torn del jugador: ")
				)
		);
		
	}
	
	private void setEventHandlers() {
		for (int j=0; j<Game.WIDTH; j++) {
			playButtons[j].setOnAction(this);
		}
		newGameButton.setOnAction(event->game.newGame());
	}

	@Override
	public void handle(ActionEvent event) {

		PlayButton btn = (PlayButton) event.getSource();
		game.play(btn.getCol());
	}
}