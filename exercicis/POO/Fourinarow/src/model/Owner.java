package model;

public enum Owner {
	NONE, GREEN, BLUE;
	
	public Owner next() {
		if (this == NONE)
			return NONE;
		else if (this == GREEN)
			return BLUE;
		else
			return GREEN;
	}
}