import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Ex1Dicc {

	public static void main(String[] args) {
		
		Set<String> mapa = new HashSet<String>();
		String[] buffer;
		String splitter;
		String compr;
		int quant;
		
		try(Scanner in = new Scanner(System.in)){
			
			System.out.print("Escriu la frase: ");
			splitter = in.nextLine();
			buffer = splitter.split(" ");
			for (int i = 0; i< buffer.length; i++){
				buffer[i] = buffer[i].replace(",", "");
				buffer[i] = buffer[i].replace(".", "");
				buffer[i] = buffer[i].toUpperCase();
			}
			mapa.addAll(Arrays.asList(buffer));
			
			Iterator<String> itr = mapa.iterator();
			
			while (itr.hasNext()){
				compr = itr.next();
				quant = 0;
				for (int i=0; i < buffer.length; i++){
					if (compr.equals(buffer[i]))
						quant++;
				}
				System.out.println(compr + " " + quant);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
}
