import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class CercaSecret {

	public static void main(String[] args) {
		
		int num;
		List<Integer> numeros = new ArrayList<Integer>();
		Scanner in = new Scanner(System.in);
		System.out.println("Introdueix el numero secret a buscar: ");
		num = in.nextInt();
		try (RandomAccessFile fitxer = new RandomAccessFile("prova.bin", "rw")){

			while(true){
				numeros.add(fitxer.readInt());
				fitxer.seek(fitxer.getFilePointer()+6);
			}
		} catch (EOFException e) {
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		num = Collections.binarySearch(numeros, num);
		if (num > 0)
			System.out.println("Index trobat a la posici� "+num);
		else
			System.out.println("No trobat");
		
		in.close();
	}
}
