import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class LlistaCompra {

	public static void main(String[] args) {
		
		Map<String, Integer> llistacompra = new HashMap<String, Integer>();
		
		llistacompra.put("tom�quets",6);
		llistacompra.put("flams", 4);
		llistacompra.put("pizzes", 2);
		llistacompra.put("llaunes de tonyina", 2);
		llistacompra.put("blat de moro", 5);
		llistacompra.put("enciam", 1);
		
		System.out.println("A partir del diccionari, imprimeix nom�s la llista d'ingredients.");
		Set<String> claus = llistacompra.keySet();
		for (String clau : claus)
		    System.out.println(clau);
		
		System.out.println("\nImprimeix la quantitat de tom�quets.");
		for (String clau : claus)
			if(clau.equals("tom�quets"))
				System.out.println(llistacompra.get(clau));
		
		System.out.println("\nImprimeix aquells ingredients dels quals n'h�gim de comprar m�s de 3 unitats.");
		for (String clau : claus)
			if(llistacompra.get(clau) >=3)
				System.out.println(clau);
		
		System.out.println("\nPregunta a l'usuari un ingredient i retorna el n�mero d'unitats que s'han de comprar. "
				+ "Si l'ingredient no �s a la llista de la compra, retorna un missatge d'error.");
		Scanner in = new Scanner(System.in);
		String compr;
		boolean trobat = false;
		System.out.print("Quin ingredient vols trobar? ");
		compr = in.nextLine();
		for (String clau : claus)
			if(clau.equals(compr)){
				System.out.println("S'han de comprar: "+llistacompra.get(clau));
				trobat = true;
			}
		if (!trobat)
			System.out.println("No s'ha trobat l'ingredient.");
		
		System.out.println("\nImprimeix tota la llista amb aquest format: ingredient (unitats a comprar)");
		
		for (String clau : claus)
		    System.out.println(clau + "("+llistacompra.get(clau)+")");
		
		System.out.println("\nA partir del diccionari, sumar totes les quantitats i donar el n�mero total d'�tems que hem de comprar. Resultat: 20.");
		int suma = 0;
		for (String clau : claus)
		    suma += llistacompra.get(clau);
		System.out.println("Hem de comprar: "+suma+" ingredients.");
		
		
		System.out.println("\nPregunta a l'usuari un ingredient, despr�s demana-li un nou valor d'unitats i modifica l'entrada corresponent al diccionari.");
		
		int change=0;
		System.out.print("Entra ingredient a modificar: ");
		compr = in.nextLine();
		System.out.print("Entra nova quantitat: ");
		change = in.nextInt();
		in.nextLine();
		for (String clau : claus)
		    if(clau.equals(compr))
		    	llistacompra.put(compr, change);
		
		System.out.println(llistacompra.get("flams"));
	}

}
