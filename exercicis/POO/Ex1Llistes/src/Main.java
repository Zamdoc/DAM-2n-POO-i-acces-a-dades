import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class Main {
	public static void main(String[] args) {
		List<String> cadenes = new ArrayList<String>();
		boolean compared=false;
		Scanner in = new Scanner(System.in);
		String cadena;
		Iterator<String> itr;
		
		do{
			System.out.println("Entra la seg�ent cadena: ");
			cadena = in.nextLine();
			if (!cadena.equals(""))
				cadenes.add(cadena);
		}while(!cadena.equals(""));
		
		System.out.println("Fi de la introducci� de cadenes.");
		String comparacio;
		
		do{
			itr = cadenes.iterator();
			System.out.println("Entra la seg�ent cadena per comparar: ");
			cadena = in.nextLine();
			while(itr.hasNext()){
				comparacio = itr.next();
				if (comparacio.equals(cadena)){
					compared = true;
				}
			}
			if (compared == true)
				System.out.println("La cadena s'ha introdu�t abans.");
			else if (compared == false && cadena != "")
				System.out.println("La cadena no s'ha introdu�t abans.");
			compared = false;
		}while(!cadena.equals(""));
		in.close();
	}
}
