package domino;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe Fitxa representa una fitxa del domino. Una fitxa tè dos
 * nombres del 0 al 6.
 */
public class Fitxa {
	/**
	 * La llista que guardarà  els dos valors de la fitxa.
	 */
	private List<Integer> valors;
	
	/**
	 * El constructor rebrà  dos nombres i inicialitzarà  la fitxa.
	 * 
	 * @param a  un dels valors de la fitxa, de 0 a 6.
	 * @param b  l'altre valor de la fitxa, de 0 a 6.
	 * @throws IllegalArgumentException  si els valors no estan entre 0 i 6.
	 */
	public Fitxa(int a, int b) throws IllegalArgumentException {
		/*
		 * TODO: inicialitza valors. Comprova que els valors d'entrada
		 * són vàlids.
		 */
		
		if (a < 0 || a > 6){
			throw new IllegalArgumentException("El primer valor és erroni");
		}
		
		if (b < 0 || b > 6){
			throw new IllegalArgumentException("El segon valor és erroni");
		}
		valors = new ArrayList<Integer>();
		valors.add(a);
		valors.add(b);

	}
	
	/**
	 * RetornarÃ  els punts que val la fitxa, Ã©s a dir, la suma dels seus valors.
	 * 
	 * @return  la suma dels valors de la fitxa.
	 */
	public int punts() {
		return get(Posicio.DRETA)+get(Posicio.ESQUERRA);
	}
	
	/**
	 * Comprova si la fitxa encaixa amb el nombre rebut o no.
	 * 
	 * @param n El nombre amb quÃ¨ es vol fer encaixar la fitxa.
	 * @return Una Posicio indicant si n Ã©s a la dreta de la fitxa,
	 * a l'esquerra, o no hi Ã©s a la fitxa.
	 */
	public Posicio encaixa(int n) {
		Posicio pos = Posicio.CAP;
		if (get(Posicio.DRETA) == n)
			pos = Posicio.DRETA;
		else if (get(Posicio.ESQUERRA) == n)
			pos = Posicio.ESQUERRA;
		return pos;
	}
	
	/**
	 * Retorna el nombre que hi ha en una de les posicions de la fitxa.
	 * 
	 * @param posicio  La posiciÃ³ de la fitxa que es vol obtenir (DRETA o ESQUERRA).
	 * @return el valor de la fitxa en aquesta posiciÃ³ (de 0 a 6).
	 * @throws IllegalArgumentException  si es consulta Posicio.CAP.
	 */
	public int get(Posicio posicio) throws IllegalArgumentException {
		int num;
		if (posicio == Posicio.CAP)
			throw new IllegalArgumentException("La posiciÃ³ ha de ser dreta o esquerra");
		if (posicio == Posicio.DRETA)
			num = valors.get(1);
		else
			num = valors.get(0);
		return num;
	}
	
	/**
	 * Capgirarà  els valors. Per exemple, si són [2|4] es guardaran com a [4|2].
	 */
	public void gira() {
		/*
		 * TODO: completa segons descripció del mètode.
		 */
		int primer = valors.get(0);
		int segon = valors.get(1);
		valors.clear();
		valors.add(segon);
		valors.add(primer);
	}
	
	/**
	 *  RetornarÃ  una cadena amb els valors de la fitxa en el format [a|b].
	 */
	@Override
	public String toString() {
		return "["+get(Posicio.ESQUERRA)+"|"+get(Posicio.DRETA)+"]";
	}
	
	/**
	 * Retorna si dues fitxes són iguals. Dues fitxes són iguals si i només si
	 * contenen els mateixos valors. L'ordre com es troben aquests valors no
	 * tè rellevància. Per exemple, [2|3] ès igual a [3|2].
	 * 
	 * @param Object o l'objecte que es compararà amb aquesta fitxa.
	 * @return true si o és una Fitxa i tè els mateixos valors que aquesta, false
	 * en cas contrari. 
	 */
	@Override
	public boolean equals(Object o) {
		/*
		 * TODO: completa segons descripció del mètode.
		 */
		boolean equals = false;
		
		if (o instanceof Fitxa) {
			if (this.hashCode() == o.hashCode()) {
				equals = true;
			}else
				equals = false;
		}
		
		return equals;
	}
	
	/**
	 * Retorna un codi hash tal que el codi hash de dues fitxes iguals segons equals()
	 * és el mateix, i és diferent si les fitxes són diferents.
	 * 
	 * @return un codi hash.
	 */
	@Override
	public int hashCode() {
		int petit = Math.min(valors.get(0), valors.get(1));
		int gran = Math.max(valors.get(0), valors.get(1));
		return petit*6+gran;
	}
}
