package domino;

import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Aquesta classe gestiona la interacció amb l'usuari per
 * tal de jugar una partida de domino amb dos jugadors (els
 * dos portats per l'usuari).
 */
public class Principal {
	/**
	 * Una partida de 2 jugadors.
	 */
	private static Partida partida = new Partida(2);
	/**
	 * Scanner per llegir l'entrada de l'usuari.
	 */
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		while (!partida.finalPartida()) {
			torn();
		}
		finalPartida();
		scanner.close();
	}
	/**
	 * Fa el torn d'un jugador.
	 */
	private static void torn() {
		Jugador j = partida.jugadorTorn();
		System.out.println("Torn del jugador "+j.getNom());
		System.out.println("Fitxes jugades: "+partida.toString());
		System.out.println("Fitxes a la mà: "+j);
		boolean correcte = false;
		while (!correcte) {
			if (partida.potJugar(j)) {
				correcte = jugada();
			} else {
				correcte = true;
				System.out.println("No pots jugar cap fitxa.");
				if (!partida.agafa()) {
					System.out.println("No hi ha fitxes per agafar, perds el torn.");
				}
			}
		}
		System.out.println();
	}
	
	private static boolean jugada() {
		boolean correcte = false;
		System.out.println("Quina fitxa vols jugar? ");
		try {
			int num1 = scanner.nextInt();
			int num2 = scanner.nextInt();
			Fitxa f = new Fitxa(num1, num2);
			scanner.nextLine();
			if (partida.tiradesPossibles(f)==2) {
				triaBanda(f);
				correcte = true;
			} else if (partida.juga(f)) {
				correcte = true;
			} else {
				System.out.println("No pots jugar aquesta fitxa.");
			}
			if (correcte) {
				System.out.println("Fitxa jugada.");
			}
		} catch (InputMismatchException | IllegalArgumentException e) {
			System.out.println("Format: num1 num2");
			scanner.nextLine();
		}
		return correcte;
	}
	
	private static void triaBanda(Fitxa f) {
		String banda = "";
		Posicio pos;
		while (!banda.equals("d") && !banda.equals("e")) {
			System.out.println("A quina banda vols jugar? (d/e)");
			banda = scanner.nextLine();
		}
		if (banda.equals("d")) {
			pos = Posicio.DRETA;
		} else {
			pos = Posicio.ESQUERRA;
		}
		partida.juga(f, pos);
	}

	/**
	 * Mostra per pantalla les puntuacions finals dels jugadors.
	 */
	private static void finalPartida() {
		System.out.println("S'ha acabat la partida");
		System.out.println("Puntuacions (com menys millor): ");
		/*
		 * TODO: consulta a partida les puntuacions dels jugadors
		 * i mostra-les en el format següent:
		 * 
		 * Jugador 1 té les fitxes [2|3] [6|6] amb puntuació 17
		 * Jugador 2 no té cap fitxa
		 */
		Map <Jugador, Integer> puntuacions = partida.getPuntuacions();
		Set<Jugador> jugadors = puntuacions.keySet();
		String llistafitxes;
		for (Jugador j : jugadors){
			llistafitxes = "";
			if (j.getFitxes().size() == 0){
				System.out.println("Jugador "+j.getNom()+" no té cap fitxa");
			}else{
				Iterator<Fitxa> itr = j.getFitxes().iterator();
				while (itr.hasNext())
					llistafitxes = llistafitxes+itr.next().toString()+" ";
				System.out.println("Jugador "+j.getNom()+" té les fitxes"+llistafitxes);
			}
		}
	}
}
