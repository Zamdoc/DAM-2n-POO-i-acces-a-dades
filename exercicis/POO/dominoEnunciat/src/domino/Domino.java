package domino;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * La classe Domino conté totes les peces d'un joc de domino.
 * Els jugadors aniran agafant peces del domino per jugar.
 */
public class Domino {
	/**
	 * Llista amb les peces que queden per agafar.
	 */
	private List<Fitxa> fitxes;
	
	/**
	 *  El constructor inicialitzarà la llista de fitxes creant totes les fitxes
	 *  d'un joc de domino, de la parella blanca (de zeros) a la parella de sisos.
	 *  Les fitxes quedaran barrejades.
	 */
	public Domino() {
		/*
		 * TODO: crea la llista de fitxes i inicialitza'l amb totes
		 * les fitxes. Utilitza Collections.shuffle() per barrejar-les.
		 */
		fitxes = new ArrayList<Fitxa>();
		for (Integer i = 0; i <= 6; i++){
			for (Integer k = i; k <= 6; k++){
				fitxes.add(new Fitxa(i, k));
			}
		}
		Collections.shuffle(fitxes);
	}
	
	/**
	 * Retornarà la primera fitxa de la llista i la traurà de la llista.
	 * Llançarà una excepció si no queden fitxes.
	 * 
	 * @return la primera fitxa de la llista.
	 * @throws IndexOutOfBoundsException si no queden fitxes.
	 */
	public Fitxa agafaFitxa() throws IndexOutOfBoundsException {
		return fitxes.remove(0);
	}
	
	/**
	 * Retornarà la quantitat de fitxes que queden.
	 * 
	 * @return la quantitat de fitxes que queden.
	 */
	public int mida() {
		return fitxes.size();
	}
	
	/**
	 *  Retornarà una cadena que mostrarà totes les fitxes que queden,
	 *  en format [a|b], on a i b són els nombres de la fitxa.
	 *  
	 *  @return una cadena que mostrarà les fitxes que queden.
	 */
	@Override
	public String toString() {
		String s = "";
		
		for (Fitxa f : fitxes)
			s += f.toString();
		
		return s;
	}
}
