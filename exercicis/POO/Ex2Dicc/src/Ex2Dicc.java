import java.util.Iterator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class Ex2Dicc {

	public static void main(String[] args) {
		
		try(Scanner in = new Scanner(System.in)){
			int num1;
			int num2;
			int buffer;
			SortedSet<Integer> mapa = new TreeSet<Integer>();
			
			System.out.print("Entra el primer numero: ");
			num1 = in.nextInt();
			in.nextLine();
			System.out.print("Entra el segon numero: ");
			num2 = in.nextInt();
			in.nextLine();
			
			for (int i = 2; i <= 1000; i++){
				mapa.add(i);
			}
			
			Iterator<Integer> itr = mapa.iterator();
			
			while (itr.hasNext()){
				buffer = itr.next();
				
				if(num1 % buffer != 0 && num2 % buffer != 0){
					itr.remove();
				}
				
			}
			
			itr = mapa.iterator();
			System.out.println("Nombres del 2 al 1000 que siguin divisors tant del primer nombre com del segon (sense repetir nombres)");
			while (itr.hasNext())
				System.out.println(itr.next());
			
			mapa.removeAll(mapa);
			
			for (int i = 2; i <= 1000; i++){
				mapa.add(i);
			}
			
			itr = mapa.iterator();
			
			while (itr.hasNext()){
				buffer = itr.next();
				
				if(num1 % buffer != 0 || num2 % buffer != 0){
					itr.remove();
				}
				
			}
			
			itr = mapa.iterator();
			System.out.println("Nombres del 2 al 1000 que siguin divisors del primer nombre o del segon (sense repetir nombres)");
			while (itr.hasNext())
				System.out.println(itr.next());
			
			
			mapa.removeAll(mapa);
			
			for (int i = 2; i <= 100; i++){
				mapa.add(i);
			}
			
			itr = mapa.iterator();
			
			while (itr.hasNext()){
				buffer = itr.next();
				
				if(!(num1 % buffer != 0 && num2 % buffer != 0)){
					itr.remove();
				}
				
			}
			
			itr = mapa.iterator();
			System.out.println("Nombres del 2 al 100 que no siguin divisor ni del primer nombre ni del segon (sense repetir nombres)");
			while (itr.hasNext())
				System.out.println(itr.next());
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
