import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Main {

	public static void main(String[] args) {
		List<String> llenguatges = new ArrayList<String>();
		
		llenguatges.add("Java");
		llenguatges.add("C");
		llenguatges.add("C++");
		llenguatges.add("Visual Basic");
		llenguatges.add("PHP");
		llenguatges.add("Python");
		llenguatges.add("Perl");
		llenguatges.add("C#");
		llenguatges.add("JavaScript");
		llenguatges.add("Delphi");
		
		System.out.println("Primera part: ");
		System.out.println(llenguatges.get(0));
		System.out.println("------------------------");
		System.out.println("Segona part: ");
		System.out.println(llenguatges.get(5));
		System.out.println("------------------------");
		System.out.println("Tercera part: ");
		ListIterator<String> itr = llenguatges.listIterator();
		int count = 0;
		
		while (count < 3){
			if (itr.hasNext())
				System.out.println(itr.next());
			count++;
		}
		System.out.println("------------------------");
		System.out.println("Quarta part: ");
		itr = llenguatges.listIterator(llenguatges.size());
		count = 0;
		while (count < 3){
			if (itr.hasPrevious())
				System.out.println(itr.previous());
			count++;
		}
		System.out.println("------------------------");
		System.out.println("Cinquena part: ");
		boolean firsttime = false;
		itr = llenguatges.listIterator();
		while(itr.hasNext()){
			if (firsttime == false){
				firsttime = true;
				itr.next();
			}else
				System.out.println(itr.next());
		}
		
		System.out.println("------------------------");
		System.out.println("Sisena part: ");
		count = 0;
		itr = llenguatges.listIterator();
		
		while(itr.hasNext()){
			System.out.println("Numero: "+count+" "+itr.next());
			count++;
		}
		System.out.println("------------------------");
		System.out.println("Setena part: ");
		System.out.println("Total de llenguatges a la llista: "+ llenguatges.size());
	}
}
