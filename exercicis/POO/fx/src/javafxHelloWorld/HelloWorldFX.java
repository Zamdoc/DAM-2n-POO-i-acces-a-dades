package javafxHelloWorld;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class HelloWorldFX extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Button btn = new Button();
		btn.setText("Saluda");

		btn.setOnAction(event -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Hola m�n");
			alert.setHeaderText(null);
			alert.setContentText("Benvingut a JavaFX!");

			alert.showAndWait();
		});
		/*
		 * btn.setOnAction(new EventHandler<ActionEvent>() {
		 *     @Override
		 *     public void handle(ActionEvent event) {
		 *         ...
		 *     }
		 * });
		 */

		StackPane root = new StackPane();
		root.getChildren().add(btn);

		Scene scene = new Scene(root, 300, 250);

		primaryStage.setTitle("Hola m�n!!");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}