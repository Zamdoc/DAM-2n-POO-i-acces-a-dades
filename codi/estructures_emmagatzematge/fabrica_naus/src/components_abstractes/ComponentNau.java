package components_abstractes;

import java.util.Set;

public abstract class ComponentNau {
	private String nom = "";
	private int pes;
	private int capacitat = 0;
	private int potencia;
	
	public abstract boolean add(ComponentNau component);
	public abstract boolean remove(ComponentNau component);
	public abstract int size();
	public abstract Set<ComponentNau> getComponents();
	public abstract String descriu();
	
	public String getNom() {
		return nom;
	}
	public int getPes() {
		return pes;
	}
	public int getCapacitat() {
		return capacitat;
	}
	public int getPotencia() {
		return potencia;
	}
	
	protected void setPes(int pes) {
		if (pes<0)
			throw new IllegalArgumentException("El pes no pot ser negatiu");
		this.pes = pes;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	protected void setCapacitat(int capacitat) {
		if (capacitat<0)
			throw new IllegalArgumentException("La capacitat no pot ser negativa");
		this.capacitat = capacitat;
	}
	protected void setPotencia(int potencia) {
		if (potencia<0)
			throw new IllegalArgumentException("La potència no pot ser negativa");
		this.potencia = potencia;
	}
	
	public int getPesTotal() {
		return getPes();
	}
	
	public int getPotenciaTotal() {
		return getPotencia();
	}
	
	public ComponentNau(int pes) {
		setPes(pes);
	}
}
