package fitxers_binaris;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class DetectorText {

	public static void main(String[] args) {
		int n;
		char c;

		if (args.length >= 1) {
			for (String nomFitxer : args) {
				try (BufferedInputStream lector = new BufferedInputStream(new FileInputStream(nomFitxer))) {
					System.out.println("Fitxer: " + nomFitxer);
					while ((n = lector.read()) != -1) {
						c = (char) n;
						if (Character.isLetterOrDigit(c) || Character.isSpaceChar(c))
							System.out.print(c);
					}
					System.out.println();
				} catch (IOException e) {
					System.err.println("ex1: error al processar " + nomFitxer + ": " + e.getMessage());
				}

			}
		} else
			System.err.println("ex1: s'esperava un o més noms de fitxer de paràmetres");
	}

}
