package examen_fitxers;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BinariAText {

	public static void main(String[] args) {
		int n;
		if (args.length==2) {
			Path origen = Paths.get(args[0]);
			Path desti = Paths.get(args[1]);
			if (Files.isRegularFile(origen) && !Files.exists(desti)) {
				try (DataInputStream entrada = new DataInputStream(new FileInputStream(origen.toString()));
						FileWriter sortida = new FileWriter(desti.toString())) {
					while (true) {
						n = entrada.readInt();
						sortida.write(n+" ");
					}
				} catch (EOFException e) {
					System.out.println("S'ha creat el fitxer.");
				} catch (IOException e) {
					System.err.println("Error d'entrada/sortida: "+e);
				}
			} else {
				System.err.println("El fitxer origen ha d'existir, i el de destí no.");
			}
		} else {
			System.err.println("Paràmetres: fitxer binari original, fitxer de text a crear.");
		}

	}

}
