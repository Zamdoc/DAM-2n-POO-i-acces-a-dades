package consultes_parametres.ex5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBAccess {
	private String url = "jdbc:mysql://localhost:3306/sakila";
	private String user = "root";
	private String passwd = "super3";
	
	private String sqlInventory = 
			"SELECT i.inventory_id, fl.title, adddate(max(r.rental_date), "
			+ "f.rental_duration), max(r.rental_date), max(r.return_date) "
			+ " FROM film f"
			+ " JOIN film_list fl ON f.film_id=fl.FID"
			+ " JOIN inventory i ON fl.FID=i.film_id"
			+ " JOIN rental r USING(inventory_id)"
			+ " JOIN store st USING(store_id)"
			+ " WHERE st.store_id = ?"
			+ " AND (1=? OR f.title LIKE ?)"
			+ " AND (1=? OR fl.actors LIKE ?)"
			+ " AND (1=? OR fl.category LIKE ?)"
			+ " GROUP BY i.inventory_id, f.title, f.rental_duration";
	
	private String sqlFilm =
			"SELECT f.title, f.description, year(f.release_year), fl.actors, "
			+ "IFNULL(ol.name, l.name) as language"
			+ " FROM film f"
			+ " JOIN film_list fl ON (f.film_id = fl.FID)"
			+ " JOIN language l USING(language_id)"
			+ " LEFT JOIN language ol ON (f.original_language_id = ol.language_id)"
			+ " JOIN inventory i ON (f.film_id = i.film_id)"
			+ " WHERE i.inventory_id = ?";
	
	public Film getFilmByInventoryId(int id) {
		Film film = null;
		try (Connection connection = DriverManager.getConnection(url,user,passwd);
				PreparedStatement statement = connection.prepareStatement(sqlFilm);){
			statement.setInt(1, id);
			try (ResultSet rs = statement.executeQuery()) {
				if (rs.next()) {
					String title = rs.getString(1);
					String description = rs.getString(2);
					String releaseYear = rs.getString(3);
					String actors = rs.getString(4);
					String language = rs.getString(5);
					film = new Film(title, description, releaseYear, actors, language);
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return film;
	}
	
	public List<Inventory> listMovies(SearchCriteria criteria, String searchString, int storeId) {
		List<Inventory> items = new ArrayList<Inventory>();
		try (Connection connection = DriverManager.getConnection(url,user,passwd);
				PreparedStatement statement = connection.prepareStatement(sqlInventory);){
			statement.setInt(1, storeId);
			statement.setInt(2, 1);
			statement.setString(3, "");
			statement.setInt(4, 1);
			statement.setString(5, "");
			statement.setInt(6, 1);
			statement.setString(7, "");
			searchString = "%"+searchString+"%";
			
			switch (criteria) {
			case TITLE:
				statement.setInt(2, 0);
				statement.setString(3, searchString);
				break;
			case ACTOR:
				statement.setInt(4, 0);
				statement.setString(5, searchString);
				break;
			case GENRE:
				statement.setInt(6, 0);
				statement.setString(7, searchString);
				break;
			}
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					int id = rs.getInt(1);
					String title = rs.getString(2);
					
					Date rentalDate = rs.getDate(4);
					Date returnDate = rs.getDate(5);
					Date dueDate=null;
					if (returnDate == null || returnDate.compareTo(rentalDate)<0)
						dueDate = rs.getDate(3);
					items.add(new Inventory(id, title, dueDate));
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return items;
	}
	
	public List<Store> getStores() {
		List<Store> stores = new ArrayList<Store>();
		try (Connection connexio = DriverManager.getConnection(url,user,passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(
						" SELECT store_id, address FROM store"+
						" JOIN address USING(address_id);");) {
			while(rs.next()){
				stores.add(new Store(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return stores;
	}
}
