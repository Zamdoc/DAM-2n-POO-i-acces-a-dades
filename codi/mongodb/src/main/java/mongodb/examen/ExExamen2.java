package mongodb.examen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class ExExamen2 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("restaurants");
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un codi postal: ");
		String zip = sc.nextLine();
		/*
		db.restaurants.aggregate([
			{$match:{"address.zipcode":"11225"}},
			{$unwind:"$grades"},
			{$match:{"grades.grade":{$ne:"Not Yet Graded"}}},
			{$group:{_id:{id:"$_id",name:"$name",address:"$address",cuisine:"$cuisine"},sc:{$avg:"$grades.score"}}},
			{$sort:{sc:1}},
			{$limit:3}])
		*/
		List<Document> restaurants = coll.aggregate(Arrays.asList(
			Aggregates.match(Filters.eq("address.zipcode", zip)),
			Aggregates.unwind("$grades"),
			Aggregates.match(Filters.ne("grades.grade", "Not Yet Graded")),
			Aggregates.group(new Document("id","$_id").append("name", "$name").append("address", "$address").append("cuisine", "$cuisine"),
					Accumulators.avg("sc", "$grades.score")),
			Aggregates.sort(Sorts.ascending("sc")),
			Aggregates.limit(5)
		)).into(new ArrayList<Document>());
		
		if (restaurants.isEmpty()) {
			System.out.println("No s'han trobat restaurants.");
		} else {
			System.out.println("Millors restaurants:");
			for (Document restaurant : restaurants) {
				Document id = restaurant.get("_id", Document.class);
				Document address = id.get("address", Document.class);
				System.out.println("Nom: "+id.getString("name")+"  Adreça: "+
						address.getString("street")+" "+address.getString("building")+"  Cuina: "+id.getString("cuisine"));
				System.out.println("Puntuació: "+restaurant.getDouble("sc"));
				System.out.println();
			}
		}
		
		sc.close();
		client.close();
	}

}
