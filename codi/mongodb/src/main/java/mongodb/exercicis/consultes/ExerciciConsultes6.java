package mongodb.exercicis.consultes;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes6 {

	public static void main(String[] args) {
		int sum=0;
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		List<Document> zips = coll.find(eq("city", "Kansas City".toUpperCase())).sort(ascending("_id")).into(new ArrayList<Document>());
		for (Document doc : zips) {
			System.out.println("Zip: "+doc.getInteger("_id")+" - Pop: "+doc.getInteger("pop"));
			sum+=doc.getInteger("pop");
		};
		System.out.println("Població total: "+sum);

		client.close();
	}

}
