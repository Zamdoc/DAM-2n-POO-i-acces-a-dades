package fungus;

import java.util.Random;

public class Dispersus extends Fungus implements Arrencador {
	private static Random RANDOM = new Random();

	public Dispersus(Colonia colonia) {
		super(colonia);
	}

	@Override
	public char getChar() {
		return '+';
	}
	
	@Override
	public void creix(Cultiu cultiu) {
		int novaFila, novaCol;
		
		for (int i=0; i<4; i++) {
			novaFila = RANDOM.nextInt(cultiu.getNFiles());
			novaCol = RANDOM.nextInt(cultiu.getNCols());
			new Dispersus(getColonia()).posa(cultiu, novaFila, novaCol);
		}
		
	}
}
