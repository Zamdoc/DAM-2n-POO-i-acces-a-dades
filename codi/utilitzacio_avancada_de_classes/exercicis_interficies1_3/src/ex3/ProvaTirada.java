package ex3;

public class ProvaTirada {
	
	public static void main(String[] args) {
		Tirada t1 = new Tirada(3, 1, 5);
		Tirada t2 = new Tirada(2, 2, 4);
		System.out.println(t1.toString());
		System.out.println(t2.toString());
		System.out.println("Comparació de les dues tirades: "+t1.compareTo(t2));
		Tirada t3 = t1.clone();
		System.out.println("Comparació d'una tirada amb una d'igual: "+t1.compareTo(t3));
	}
}
