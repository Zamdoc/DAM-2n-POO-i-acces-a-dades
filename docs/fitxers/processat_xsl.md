## Processat de fitxers XML amb XSL

Un altre aspecte relacionat amb els fitxers XML és la seva transformació a
altres format un fitxer d'esquema XSL.

Això també ho podem fer en Java. El següent exemple converteix les dades d'un
fitxer XML molt senzill a un fitxer HTML:

[Processat XSL](codi/fitxers/exemple_processat_xsl/src/exemple_processat_xsl/Conversor.java)

El codi és simple: tenim dues fonts de dades, el fitxer XML i el fitxer XSL, i
un resultat a obtenir, el fitxer HTML.

Creem un objecte *Transformer*, passant-li la referència a l'estil que ha
d'aplicar.

Finalment, apliquem la transformació.
