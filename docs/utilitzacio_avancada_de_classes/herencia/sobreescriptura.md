#### Sobreescriptura de mètodes

***Exemple:***

```java
public class ClasseBase {
   public int variablePublic;

   // (...)

   public void metodePublic() {
       System.out.println();
   }
}

public class ClasseDerivada extends ClasseBase {
   public int var;

   public void metode() {
       // (...)
   }
}

public class Principal {
   public static void main(String[] args) {
       ClasseDerivada obj = new ClasseDerivada();
       obj.variablePublic = 5;
       obj.metodePublic();
       obj.var = 5;
       obj.metode();
       System.out.println(obj.toString());
   }
}
```

En aquest exemple podem veure clarament com es pot cridar una variable o
mètode que pertanyi a qualsevol de les classes superiors de la classe de
l'objecte. L'objecte *obj* és de tipus *ClasseDerivada*, i estem cridant
a *variablePublic* i *metodePublic* que són atributs de *ClasseBase*, a
*var* i *metode* que ho són de *ClasseDerivada*, i a *toString* que és
d'*Object*. Abans hem vist com això també es podia fer des de dins del
codi de la classe *ClasseDerivada*.

El compilador sap on anar a buscar cada variable i cada mètode perquè no
hi ha confusió possible: els noms són únics en aquesta jerarquia de
classes.

**Sobreescriure un mètode** consisteix en declarar un mètode amb el mateix
nom i paràmetres que un mètode que ja existeix a una classe superior. El
tipus que retorna pot ésser el mateix que a la classe superior, o bé
d'una classe derivada del que es retorna a la classe superior.

***Exemple:***

```java
public class ClasseBase {
   // (...)

   public void metodePublic() {
       System.out.println("ClasseBase");
   }
}

public class ClasseDerivada extends ClasseBase {
   // (...)
   @Override
   public void metodePublic() {
       // mètode sobreescrit
       System.out.println("ClasseDerivada");
   }
}

public class Principal {
   public static void main(String[] args) {
       ClasseDerivada obj1 = new ClasseDerivada();
       obj1.metodePublic();

       ClasseBase obj2 = new ClasseBase();
       obj2.metodePublic();

       ClasseBase obj3 = new ClasseDerivada();
       obj3.metodePublic();

       // ClasseDerivada obj4 = new ClasseBase();
       // Ep! Que aquesta és incorrecta!

       Object obj5 = new ClasseDerivada();
       // obj5.metodePublic();
       // Incorrecte!! metodePublic no existeix a Object
   }
}
```

El comportament en cas de mètodes sobreescrits és cridar sempre el
mètode de la classe més concreta, és a dir, de la que sigui d'un nivell
inferior en la jerarquia de classes.

Fixeu-vos que el compilador no pot saber quin és el mètode que s'acabarà
cridant. En els exemples anteriors, el tercer cas ho exemplifica:

```java
       ClasseBase obj3 = new ClasseDerivada();
       obj3.metodePublic();
```

*obj3* és una referència de tipus *ClasseBase*, però l'objecte realment
és de tipus *ClasseDerivada*. Quan es fa la crida a *metodePublic*, el
compilador no pot saber quin dels dos mètodes s'ha de cridar.

Un cas més clar seria aquest:

```java
public void metode(ClasseBase obj) {
   obj.metodePublic;
}
```

En temps de compilació no se sap de quin tipus serà *obj*: pot ser de
*ClasseBase*, de *ClasseDerivada*, o d'una altra classe que derivi de
*ClasseBase* que ni tan sols s'hagi escrit encara quan es compila aquest
codi. Per tant, el compilador només s'encarrega de garantir que el
mètode existirà, però la crida exacte al mètode es decideix en temps
d'execució, depenent del tipus concret que tingui en aquell moment
*obj*.

El compilador sí que pot garantir que *metodePublic* existirà, perquè
sap que està definit a *ClasseBase* i totes les classes que en derivin
tindran, com a mínim, aquesta implementació. El cinquè exemple és
incorrecte perquè el compilador no pot assegurar que el mètode cridat
existeixi per a *obj5*.

De la mateixa manera que en el cas dels constructors, des d'un mètode
sobreescrit es pot cridar el mateix mètode de la classe base utilitzant
la paraula *super*. Això és útil si el codi del mètode de la classe
derivada amplia però no substitueix el codi de la classe base.

***Exemple:***

Imaginem que volem crear una etiqueta gràfica que automàticament es posi
opaca quan se li canvia el color de fons, i així no haver de recordar-ho
cada vegada. Podríem fer alguna cosa com:

```java
public class Etiqueta extends JLabel {
[...]
   @Override
   public void setBackground(Color bg) {
       setOpaque(true);
       super.setBackground(bg);
   }
}
```

Noteu que l'ús de *super* aquí indica explícitament que s'ha de cridar el
mètode *setBackground* de la classe superior. Si no ho poséssim estaríem
cridant el mateix mètode, cosa que provocaria una recurrència infinita.

No s'ha de confondre la sobreescriptura de mètodes amb la
*sobrecàrrega*. **Sobrecarregar un mètode** consisteix en crear un mètode
amb el mateix nom però diferents paràmetres que un mètode existent,
sigui a la mateixa classe o a un classe derivada. En aquest cas, el
compilador sempre pot saber a quin dels mètodes s'està cridant a partir
del tipus dels paràmetres que es passen. En canvi, un mètode sobreescrit
té el mateix nom i paràmetres que el primer mètode, i s'ha de situar
obligatòriament a una classe derivada de la que conté el mètode
original.

Quan sobreescrivim mètodes, és útils afegir l'anotació ***@Override***
just abans de la seva declaració. D'aquesta forma el compilador
coneixerà la nostra intenció de sobreescriure, i ens avisarà si per un
error tipogràfic o de tipus de dades resulta que no ho estem fent i en
comptes d'això estem creant un mètode completament nou. Sense
l'*Override* aquesta equivocació passaria desapercebuda.

Finalment, la visibilitat d'un mètode sobreescrit pot ser major que la
del mètode original, però no a l'inrevés. Per exemple, el mètode
original pot ser *protected* i el mètode sobreescrit ser *public*.
